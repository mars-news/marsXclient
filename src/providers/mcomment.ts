/**
 * Created by meryn on 2017/06/13.
 */


import { Injectable } from '@angular/core'
import { MUrls } from './murl'
import { MNet } from './mnet'
import { MCache } from './mcache'
import { MUtils } from '../common/mutils'
import { mfuncs } from "../common/mfuncs";

import { MError, ErrorCode } from '../common/merror'

import 'rxjs/add/operator/map'

import {Comment, Vote} from "../interfaces/comment";

const Max_Score = 10;
const Open_News_Min_Score = 2;
const Comment_Add_Score = 2;
const Vote_Add_Score = 1;


class CommentSet{

  tm: Date;
  comments: Comment[];
  set: Set<number>;

  constructor(){

    this.tm = new Date();
    this.comments = [];
    this.set = new Set();

  }

}

class ReplySet{

  tm: Date;
  replies: Comment[];
  set: Set<number>;

  constructor(){

    this.tm = new Date();
    this.replies = [];
    this.set = new Set();

  }

}


class VoteSet{

  tm: Date;
  votes: Vote[];
  set: Set<string>;

  constructor(){

    this.tm = new Date();
    this.votes = [];
    this.set = new Set();

  }

}



@Injectable()
export class MComment {

  commentsAll = new Map<number, CommentSet>();
  replyAll = new Map<string, ReplySet>();
  voteAll = new Map<number, VoteSet>();
  scoreAll = new Map<number, number>();


  commentRefreshDuration = 1 * 3600;

  constructor(public mnet: MNet,
              public cache:MCache,
              private murls: MUrls){
  }

  getReplyKey(nid:number, pid:number): string{
    return nid + "-" + pid;
  }

  getVoteKey(pid:number, rid:number): string{
    return pid + "-" + rid;
  }


  containsComment(nid: number): boolean{    //there is code that delete old keys

    if(!this.commentsAll.has(nid)){
      return false;
    }

    let now = new Date();

    let commentItem = this.commentsAll.get(nid);

    if(MUtils.twoDateDiffInSec(now, commentItem.tm) > this.commentRefreshDuration){
      this.commentsAll.delete(nid);
      return false;
    }

    return true;

  }

  containsReply(nid: number, pid: number): boolean{    //there is code that delete old keys

    let key = this.getReplyKey(nid, pid);

    if(!this.replyAll.has(key)){
      return false;
    }

    let now = new Date();

    let replyItem = this.replyAll.get(key);

    if(MUtils.twoDateDiffInSec(now, replyItem.tm) > this.commentRefreshDuration){

      this.commentsAll.delete(nid);
      return false;

    }

    return true;

  }


  addComment(nid: number, comment:Comment): Promise<any>{

    return new Promise((resolve, reject) => {

      resolve(this.cache.getVote(nid, comment.Id, 0, false));

    }).then((val) => {

      let voteCount = <number>val;
      if(!val){
        voteCount = 0;
      }

      mfuncs.log("vote count:" + voteCount);

      if(!this.containsComment(nid)){
        let newSet = new CommentSet();

        this.commentsAll.set(nid, newSet);
      }

      let set = this.commentsAll.get(nid).set;

      if(!set || set.has(comment.Id)){
        return false;
      }

      if(voteCount){
        comment.HasVote = true;

        if(voteCount > comment.Vote){
          comment.Vote = voteCount;
        }
      }


      this.commentsAll.get(nid).set.add(comment.Id);
      this.commentsAll.get(nid).comments.push(comment);
      this.commentsAll.get(nid).tm = new Date();

      return true;

    });

  }


  addReply(nid: number, pid: number, reply:Comment): Promise<any>{

    return new Promise((resolve, reject) => {

      return this.cache.getVote(nid, pid, reply.Id, true).then((val) => {
        resolve(val);
      })

    }).then((val)=>{

      let voteCount = <number>val;

      let key = this.getReplyKey(nid, pid);

      if(!this.containsReply(nid, pid)){
        let newSet = new ReplySet();

        this.replyAll.set(key, newSet);
      }

      let set = this.replyAll.get(key).set;

      if(!set || set.has(reply.Id)){
        return false;
      }

      if(voteCount){
        reply.HasVote = true;
        if(voteCount > reply.Vote){
          reply.Vote = voteCount;
        }
      }

      this.replyAll.get(key).set.add(reply.Id);
      this.replyAll.get(key).replies.push(reply);
      this.replyAll.get(key).tm = new Date();

      this.addScore(nid, Comment_Add_Score);

      return true;

    });

  }


  getComments(nid: number): Promise<Comment[]>{

    if(this.containsComment(nid)){

      return Promise.resolve(this.commentsAll.get(nid).comments)

    }

    return this.refreshComment(nid);
  }

  getReplies(nid: number, pid: number): Promise<Comment[]>{

    let key = this.getReplyKey(nid, pid);

    if(this.containsReply(nid, pid)){
      return Promise.resolve(this.replyAll.get(key).replies);
    }

    return this.refreshReply(nid, pid);
  }


  getRepliesCount(nid: number, pid:number): number{

    let key = this.getReplyKey(nid, pid);

    if(!this.containsReply(nid, pid)){
      return 0;
    }

    return this.replyAll.get(key).replies.length;

  }

  getCommentsCount(nid: number): number{

    if(!this.containsComment(nid)){

      return 0;

    }

    return this.commentsAll.get(nid).comments.length;
  }

  cleanCommentAndRepliesRelateToUser(uid:number):Promise<any> {

    return this.cache.getUserInfo().then((uinfo)=>{
      if(uinfo.uid != uid){
        this.cache.blockUser(uid, 0);
      }
    }).then(() => {

      let keys = Array.from(this.commentsAll.keys());
      return keys.reduce((promise, key, currentIndex) => {
        return promise.then(() => {

          let commentSet = this.commentsAll.get(key);
          let dels: number[] = [];

          if(!commentSet){

            return

          }else{

            return commentSet.comments.reduce((promise, val, currentIndex) => {

              return promise.then(() => {

                let comment_one = val;

                return this.cache.checkIfUserOrCommentBlocked(comment_one.Nid, comment_one.Id, 0, comment_one.Uid).then((ok) => {
                  if (ok == true) {
                    dels.push(currentIndex);
                  }
                })
              })

            }, Promise.resolve()).then(() => {

              mfuncs.log("dels comments:" + JSON.stringify(dels));

              for (let ii = 0; ii < dels.length; ii++) {
                commentSet.comments.splice(dels[ii], 1)
              }
              this.commentsAll.set(key, commentSet)
            });
          }
        })

      }, Promise.resolve())

    }).then(() => {

      let rkeys = Array.from(this.replyAll.keys());

      let replyalltmp = this.replyAll;

      rkeys.reduce((promise, key, currentIndex) => {
        return promise.then(() => {

          let replySet = this.replyAll.get(key);
          let dels: number[] = [];

          if(!replySet){
            return
          }else {

            return replySet.replies.reduce((promise, val, currentIndex) => {

              return promise.then(() => {

                let reply_one = val;

                return this.cache.checkIfUserOrCommentBlocked(reply_one.Nid, reply_one.Pid, reply_one.Id, reply_one.Uid).then((ok) => {
                  if (ok == true) {
                    dels.push(currentIndex);
                  }
                })
              })

            }, Promise.resolve()).then(() => {

              mfuncs.log("dels replies:" + JSON.stringify(dels));

              for (let ii = 0; ii < dels.length; ii++) {
                replySet.replies.splice(dels[ii], 1)
              }
              this.replyAll.set(key, replySet)
            })

          }


        })
      }, Promise.resolve());

    })
  }




  reportComment(nid:number, pid:number, uid:number): Promise<any>{
    return this.cache.blockComment(nid, pid, 0, 0).then((ok) => {
      if(ok == false){
        return false
      }else{
        return this.cleanCommentAndRepliesRelateToUser(uid);
      }
    });
  }

  reportReply(nid:number, pid:number, rid:number, uid:number): Promise<any>{

    return this.cache.blockComment(nid, pid, rid, 0).then((ok) => {
      if(ok == false){
        return false
      }else{
        return this.cleanCommentAndRepliesRelateToUser(uid);
      }
    });
  }


  refreshComment(nid: number): Promise<Comment[]>{

    let offset = 1;
    if(this.containsComment(nid)){
      offset = this.getCommentsCount(nid) + 1;
    }
    if(offset <= 0){
      offset = 1;
    }

    return this.mnet.send(this.murls.getCommentRefreshUrl(), {nid: nid, offset: offset}, ["Stat", "Comments"]).then((rsp) => {

      let comments_rsp = rsp.Comments;

      return  comments_rsp.reduce((promise, val, currentIndex) =>{

        return promise.then(() =>{

          let comment_one = val;
          let newComment = Comment.initFromJObj(comment_one);

          return this.cache.checkIfUserOrCommentBlocked(newComment.Nid, newComment.Id, 0, newComment.Uid).then((ok) => {
            if(!ok){
              return this.addComment(nid, newComment);
            }
          })
        })

      }, Promise.resolve())

    }).then(() => {

      if(this.containsComment(nid)){
        return Promise.resolve(this.commentsAll.get(nid).comments)
      }else{
        return Promise.resolve([]);
      }

    })
  }


  refreshReply(nid: number, pid: number): Promise<Comment[]>{

    let offset = 1;
    if(this.containsReply(nid, pid)){
      offset = this.getRepliesCount(nid, pid) + 1;
    }

    if(offset <= 0){
      offset = 1;
    }

    return this.mnet.send(this.murls.getReplyRefreshUrl(), {nid: nid, pid: pid, offset: offset}, ["Stat", "Replies"]).then((rsp) => {

      let replies_rsp = rsp.Replies;

      return  replies_rsp.reduce((promise, val, currentIndex) =>{

        return promise.then(() =>{

          let reply_one = val;
          let newReply = Comment.initFromJObj(reply_one);

          return this.cache.checkIfUserOrCommentBlocked(newReply.Nid, newReply.Pid, newReply.Id, newReply.Uid).then((ok) => {
            if (!ok) {
              return this.addReply(nid, pid, newReply);
            }
          })
        })

      }, Promise.resolve())

    }).then((ok) => {
      let replySet = this.replyAll.get(this.getReplyKey(nid, pid));
      if(!replySet){

        return Promise.resolve([]);

      }
      return Promise.resolve(replySet.replies);
    })

  }

  addScoreFromComment(nid:number){
    this.addScore(nid, Comment_Add_Score);
  }


  addScore(nid:number, score:number){

    if(!this.scoreAll.get(nid)){
      this.scoreAll.set(nid, Open_News_Min_Score + score);
      return
    }

    this.scoreAll.set(nid, this.scoreAll.get(nid) + score);
  }


  getScore(nid: number): number{

    if(!this.scoreAll.get(nid)){
      return 0;
    }
    return this.scoreAll.get(nid);

  }


  addVote(nid: number, pid: number, rid: number, isReply:boolean):Promise<any>{   //used for feedback to server

    return this.cache.getVote(nid, pid, rid, isReply).then((val) => {
      if(val){
        mfuncs.log("already vote for:" + nid + "," + pid + "," + rid);
        return;
      }

      let key = this.getVoteKey(pid, rid);

      if(!this.voteAll.has(nid)){
        this.voteAll.set(nid, new VoteSet())
      }

      let voteSet = this.voteAll.get(nid);

      if(voteSet.set.has(key)){
        return;
      }

      voteSet.votes.push(new Vote(pid, rid, isReply));
      voteSet.set.add(key);


      //this.commentsAll.get(nid).comments

      let voteNum = 1;

      if(isReply && this.replyAll.get(this.getReplyKey(nid, pid))){

        for(let rpy of this.replyAll.get(this.getReplyKey(nid, pid)).replies){
          if(rpy.Id == rid){
            rpy.HasVote = true;
            rpy.Vote += 1;
            voteNum = rpy.Vote;
          }
        }

      }else if(this.commentsAll.get(nid)){

        for(let cmt of this.commentsAll.get(nid).comments){
          if(cmt.Id == pid){
            cmt.HasVote = true;
            cmt.Vote += 1;
            voteNum = cmt.Vote;
          }
        }
      }

      return this.cache.saveVote(nid, pid, rid, isReply, voteNum);


    });


  }



  SendScoreAndVotes(nid: number){

    let voteStr = "";

    if(this.voteAll.has(nid)){
      voteStr = JSON.stringify(this.voteAll.get(nid).votes);
    }else{
      voteStr = "[]"
    }

    let score = this.getScore(nid);

    if(score < Open_News_Min_Score){
      score = Open_News_Min_Score;
    }

    return this.mnet.send(this.murls.getVoteUrl(), {nid: nid, score: score, votes: voteStr}, ["Stat"]).then((rsp) => {

      let stat = rsp.Stat;

      if(stat == 0){
        mfuncs.log("vote sent ok");
      }else{
        mfuncs.log("vote sent error");
      }
    }).catch((err) => {
      mfuncs.log("err:" + JSON.stringify(err));
    })


  }

}




























