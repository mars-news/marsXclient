/**
 * Created by meryn on 2017/05/01.
 */

import { Injectable } from '@angular/core'
import {UserData, UserNameExtInfo} from '../interfaces/user-data'
import { MCache } from './mcache'
import { MUrls } from './murl'
import { MDef } from '../common/mdefs'
import { MNet } from './mnet'
import {ErrorCode, MError} from "../common/merror";
import { mfuncs } from "../common/mfuncs";





@Injectable()
export class MUser{

  uInfo: UserData;

  constructor(public cache: MCache, public murls: MUrls, public mnet:MNet){}

  loadUserInfo(): Promise<UserData>{

    return this.cache.getUserInfo().then((val) => {

      this.uInfo = <UserData>val;
      if(!this.uInfo || this.uInfo.uid <= 0){
        throw new Error("no user info");
      }

      return this.uInfo;
    })

  }

  getExtInfo(): Promise<UserNameExtInfo>{

    return this.getUinfo().then((uinfo) => {

      mfuncs.log("uinfo:" + JSON.stringify(uinfo));
      if(!uinfo){
        return null;
      }

      return UserNameExtInfo.GetUserNameExtInfoFrom(uinfo.name);
    })

  }


  getUinfo(): Promise<any>{

    if(this.uInfo){

      return Promise.resolve().then(() => {

        return this.uInfo;

      });

    }else{

      return this.loadUserInfo();

    }

  }

  setUinfo(info: UserData): Promise<number>{

    this.uInfo = info;

    return this.cache.saveUserInfo(info).then((saveTm) => {

      if(saveTm){
        mfuncs.log("save uinfo successed:" + JSON.stringify(info));

        return 0
      }

      throw new Error("cache set uinfo failed")
    })

  }




  userRegister(interests: string): Promise<UserData>{

    let info: UserData;

    return this.mnet.send(this.murls.getRegisterUrl(), {interests: interests}, ["Name", "Token", "Id"])
      .then((rsp) => {

        let uInfo = new UserData();

        uInfo.uid = rsp.Id;
        uInfo.interestStr = interests;
        uInfo.name = rsp.Name;


        uInfo.photoUrl = MDef.Default_Photo_Url;
        uInfo.token = rsp.Token;

        info = uInfo;

        return uInfo;

      }).then((uInfo) => {

        return this.setUinfo(uInfo);

      }).then((val) => {
        return info;
      });

  }


  userLogin(): Promise<boolean>{
      return this.getUinfo()
        .then((info) => {

          return this.mnet.send(this.murls.getLoginUrl(), {Uid: info.uid, Pwd: info.token}, []).then((rsp) => {
            return true
          })

        }).then((val) => {

          return true

        })
  }


  userUpdateName(newName: string): Promise<any>{

    let nName:string;

    return this.mnet.send(this.murls.getUserInfoUpdateUrl(), {Name: newName}, ['NewName']).then((rsp) => {

      if(rsp.NewName.length <= 0){
        throw new MError(ErrorCode.Response_Field_Wrong, "new name length 0");
      }

      nName = rsp.NewName;  //full name with ext info

      return this.getUinfo();

    }).then((info) => {

      let uinfo = <UserData>info;
      if(!uinfo){
        throw new MError(ErrorCode.Value_Missing, "get u info failed");
      }

      uinfo.name = nName;

      return this.setUinfo(uinfo);

    });
  }


  getMyPhotoVersion(): Promise<number>{

     return this.loadUserInfo().then((uinfo) => {
       return this.cache.getPhotoImageVersion(uinfo.uid);
     })
  }

  incrMyPhotoVersion(): Promise<number>{

     return this.loadUserInfo().then((uinfo) => {
       return this.cache.incrPhotoImageVersion(uinfo.uid);
     });

  }


}











