/**
 * Created by meryn on 2017/08/15.
 */
import {Injectable} from "@angular/core";
import { MNet } from "../providers/mnet";
import { mfuncs } from "../common/mfuncs"

@Injectable()
export class MWords {

  wordList: string[];


  constructor(private mnet: MNet){

    this.wordList = [];

  }

  loadWordsFile(){

    this.mnet.loadFile('assets/blockwords/ja').then((content) => {

      this.wordList = content.split(/\r?\n/);

      mfuncs.log("load black words finished")

    })
  }

  filterTxt(txt:string): string{

    for(let ii= 0; ii < this.wordList.length; ii++ ){
      txt = txt.replace(this.wordList[ii], "＊＊＊")
    }

    return txt;
  }







}
