/**
 * Created by meryn on 2017/06/20.
 */
import { Storage } from '@ionic/storage';
import {Injectable} from "@angular/core";
import {ErrorCode, MError} from "../common/merror";
import { MUtils } from '../common/mutils'
import { mfuncs } from "../common/mfuncs";


export enum DataType{
  Simple,
  General,
}

class Table{
  type: DataType;
  name: string;
  cls: any;
  keyName: string;
  ftbName: string;
  fkeyName: string;
  hasParent: boolean;

  gcTime: number; //seconds
  gcKeepCount: number;
  gcEachKeyFunc: any; //function(key)


  children: string[];
  addToListAsc: boolean;
  constructor(){
    this.children = [];
    this.addToListAsc = true;
  }
  /*
  getIdWithPrefix(key: string): string{

    return key.substring(this.name.length)

  }*/

}


class RootListKey{

  tm: Date;
  key: string;
  fkey: string;

  constructor(key: string, fk: string, dt: Date){
    this.tm = dt;
    this.key = key;
    this.fkey = fk;
  }

  static FromJObj(jobj){

    let ti = new RootListKey(null, null, null);

    ti.tm = new Date(jobj['tm']);
    ti.key = jobj['key'];
    ti.fkey = jobj['fkey'];

    return ti;
  }
}

@Injectable()
export class Mdb{

  tables  = new Map<string, Table>();

  constructor(public storage: Storage){
  }

  registerTable(tableName: string, tableClass: any, ftableName:string, fkeyName:string, type:DataType, insertToListAsc: boolean = true){

    let table = new Table();

    table.name = tableName;
    table.ftbName = ftableName;
    table.fkeyName = fkeyName;
    table.type = type;
    table.cls = tableClass;
    table.addToListAsc = insertToListAsc;

    if(table.ftbName.length > 0 && table.fkeyName.length > 0){
      table.hasParent = true;
    }

    this.tables.set(tableName, table);
  }

  registerGcDuration(tableName:string, secs: number){

    let tb = <Table>this.tables.get(tableName);

    tb.gcTime = secs;

  }

  registerGcEachKeyFunc(tableName:string, handler: any){

    let tb = <Table>this.tables.get(tableName);

    tb.gcEachKeyFunc = handler;
  }

  registerGcKeepCount(tableName:string, keepCount:number){

    let tb = <Table>this.tables.get(tableName);

    tb.gcKeepCount = keepCount;
  }


  afterRegister():Promise<boolean>{

    return new Promise((resolve, reject) => {

      let keys = Array.from(this.tables.entries());

      for(let key of keys){
        let tb = key[1];

        if(tb.ftbName && tb.ftbName.length > 0){
          if(!this.tables.has(tb.ftbName)){
            throw new MError(ErrorCode.Value_Missing, "error: no foreign table:" + tb.ftbName);
          }

          this.tables.get(tb.ftbName).children.push(key[0]);
        }
      }

      resolve(true);

    });
  }


  gcOne(tbName: string):Promise<any>{

    mfuncs.log("gc one start...");

    let tb = <Table>this.tables.get(tbName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tbName);
    }


    if(tb.gcTime && tb.gcTime > 0) {
      return this.gcTable(tb);
    }else{
      return Promise.resolve(null);
    }

  }


  gc():Promise<any>{

    mfuncs.log("gc start...");

    let entries = Array.from(this.tables.entries());

    return entries.reduce((promise, item, currentIndex) =>{

      let tb = item[1];

      return promise.then(() => {
        if(tb.gcTime && tb.gcTime > 0) {
          return this.gcTable(tb);
        }else{
          return null;
        }
      })
    }, Promise.resolve());

  }


  gcTable(tb: Table):Promise<any>{

    mfuncs.log("gc table:" + tb.name);

    let listKey = this.getListKeyName(tb);

    let now = new Date();

    let items: RootListKey[];

    return this.storage.get(listKey).then((val) =>{

      mfuncs.log("list tb:"+ tb.name);

      items = <RootListKey[]>val;
      if(!items){
        return null;
      }

      let dels = [];
      for(let ii = 0; ii < items.length; ii++){

        let item = items[ii];

        if(!item.tm){
          dels.unshift(ii);
        }

        item.tm = new Date(item.tm);

        if(!item.tm || now.getTime() - item.tm.getTime() > tb.gcTime * 1000){
          dels.unshift(ii);
        }

        mfuncs.log("item id:" + item.key + " now:" + now + " item tm:" + item.tm + " diff:" + (now.getTime() - item.tm.getTime()) + " gcTime:" + (tb.gcTime * 1000) + "dels:" + dels);

      }

      if(tb.gcKeepCount){
        for(let ii = tb.gcKeepCount; ii < items.length; ii++){    //check
          if(dels.indexOf(ii) < 0){
            dels.unshift(ii);
          }
        }
      }

      return dels;
    }).then((val) => {

      let delArr = <number[]>val;
      if(!delArr){
        return null;
      }

      return delArr.reduce((promise, delIdx, currentIndex) =>{

        let item = items[delIdx];

        return promise.then(() => {
          if(tb.gcEachKeyFunc){
            tb.gcEachKeyFunc(item.key);  //check
          }

          return this.removeData(tb.name, item.key);   // list is changed here
        });
      }, Promise.resolve([])).then(() => {
        return delArr;
      })

    }).then((val) => {

      if(!val){
        return null;
      }

      let delArr = <number[]>val;
      for(let ii = 0; ii < delArr.length; ii++){

        items.splice(delArr[ii], 1);
      }

    })
  }


  saveFunctionLock = false;

  saveFunctionCallbacks = [];

  saveData(tableName: string, key: any, fkey: any, data: any):Promise<any>{

    if(this.saveFunctionLock){
      mfuncs.log('save data locked:' + tableName + ' ' + key + ' ' + fkey + ' ' + " len:" + this.saveFunctionCallbacks.length);
      this.saveFunctionCallbacks.push(() =>{
        return this.saveDataFunc(tableName, key, fkey, data);
      });

      return Promise.resolve(true);
    }else {
      mfuncs.log('save data excuted:' + tableName + ' ' + key + ' ' + fkey);  //, data);
      this.saveFunctionLock = true;

      this.saveFunctionCallbacks.push(() =>{
        return this.saveDataFunc(tableName, key, fkey, data);
      });

      return this.exhaustFuncs();

    }
  }


  exhaustFuncs():Promise<any>{

    mfuncs.log('exhaust begin:' + this.saveFunctionCallbacks.length);

    let promise = Promise.resolve();

    while(this.saveFunctionCallbacks.length > 0){
      let fitem = this.saveFunctionCallbacks.pop();
      mfuncs.log('pop after ' + this.saveFunctionCallbacks.length);

      promise = promise.then(fitem);
    }

    return promise.then(() => {
      if(this.saveFunctionCallbacks.length > 0){
        mfuncs.log('exhaust not over ' + this.saveFunctionCallbacks.length);
        return this.exhaustFuncs();
      }else{
        mfuncs.log('exhaust over');
        this.saveFunctionLock = false;
        return true;
      }
    });
  }


  saveDataFunc(tableName: string, key: any, fkey: any, data: any):Promise<any>{
    let tb = <Table>this.tables.get(tableName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tableName);
    }

    switch(tb.type){
      case DataType.General:
        return this.saveGeneralData(tb, key, fkey, data);
      case DataType.Simple:
        return this.saveSimpleData(tb, data);
      default:
        throw new MError (ErrorCode.Value_Missing, "data type no existed" + tb.type);
    }

  }


  getData(tableName: string, key: any):Promise<any>{

    let tb = <Table>this.tables.get(tableName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tableName);
    }

    switch(tb.type){
      case DataType.General:
        if(tb.cls){
          return this.getGeneralObjData(tb, key);
        }else{
          return this.getGeneralStringData(tb, key).then((val) => {
            //mfuncs.log('general string data ret:' + val);
            return val;
          })
        }

      case DataType.Simple:
        return this.getSimpleObjData(tb);
      default:
        throw new MError (ErrorCode.Value_Missing, "data type no existed" + tb.type);
    }
  }

  getAllData(tableName:string): Promise<any[]>{

    let tb = <Table>this.tables.get(tableName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tableName);
    }

    switch(tb.type){
      case DataType.General:
        return this.getGeneralObjDataAll(tb);
      default:
        throw new MError (ErrorCode.Value_Missing, "data type no existed" + tb.type);
    }
  }

  updateData(tableName:string, key: any, changeFunc:(x: any)=>Promise<any>):Promise<any>{

    let tb = <Table>this.tables.get(tableName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tableName);
    }

    switch(tb.type){
      case DataType.General:
        return this.updateGeneralData(tb, key, changeFunc);
      default:
        throw new MError (ErrorCode.Value_Missing, "data type no existed" + tb.type);
    }
  }

  simpleUpdateData(tableName:string, key: any, data: any):Promise<any>{

    let tb = <Table>this.tables.get(tableName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tableName);
    }

    switch(tb.type){
      case DataType.General:
        return this.simpleUpdateGeneralData(tb, key, data);
      default:
        throw new MError (ErrorCode.Value_Missing, "data type no existed" + tb.type);
    }
  }

  removeData(tableName:string, key: any):Promise<any>{

    let tb = <Table>this.tables.get(tableName);

    if(!tb){
      throw new MError(ErrorCode.Table_Missing, "table is not exists:" + tableName);
    }

    mfuncs.log("rm table:" + tableName + " del:" + key);

    switch(tb.type){
      case DataType.General:
        return this.removeGeneralData(tb, key);
      default:
        throw new MError (ErrorCode.Value_Missing, "data type no existed" + tb.type);
    }
  }


  private getKeyNameByTable(tb: Table, key:any): string{
    return tb.name + key;
  }

  private getKeyName(tbname:string, key:any): string{
    return tbname + key;
  }

  private getListKeyName(tb: Table): string{
    return tb.name + '_list';
  }


  private addToRootList(tb:Table, key: any, fkey: any):Promise<any>{

    let listKey = this.getListKeyName(tb);

    let newTimeKey = new RootListKey(key, fkey, new Date());

    return this.storage.get(listKey).then((val) =>{

      let items = <RootListKey[]>val;
      if(!items){
        items = [];
      }

      if (!items.find((ele) => {
          return ele.key == newTimeKey.key;
        })) {
        if(tb.addToListAsc){
          items.push(newTimeKey);
        }else{
          items.unshift(newTimeKey);
        }

      }

      return this.storage.set(listKey, items);
    })
  }


  private getTable(tbName: string):Promise<any>{

    let tb = this.tables.get(tbName);
    if(!tb){
      throw new MError(ErrorCode.Value_Missing, "table is not exists:" + tbName);
    }

    return Promise.resolve(tb);
  }


  private getRootList(tb:Table):Promise<RootListKey[]>{

    let listKey = this.getListKeyName(tb);

    return this.storage.get(listKey).then((val) => {
      if (!val) {
        return [];
      }

      let items:any[];
      let res = [];


      if(MUtils.isString(val)){
        items = JSON.parse(val);
      }else{
        items = val;
      }

      //let jobjs = JSON.parse(val);

      for (let ii = 0; ii < items.length; ii++) {

        let item = items[ii];
        let tItem = RootListKey.FromJObj(item);

        if (!res.find((ele) => {
            return ele.key == tItem.key;
          })) {
          res.push(tItem)
        }
      }

      return res;
    });
  }

  private getGeneralObjDataAll(tb: Table):Promise<any[]>{

    return this.getRootList(tb).then((list) => {

      return list.reduce((promise, item, currentIndex) =>{

        return promise.then((arr) =>{

          return this.getGeneralObjData(tb, item.key).then((objVal) => {

            if(objVal){
              arr.push(objVal);
            }

            return arr;

          })
        })
      }, Promise.resolve([]));

    });
  }




  private deleteFromRootListByKey(tb:Table, key:string, foreign: boolean){

    let listKey = this.getListKeyName(tb);

    return this.getRootList(tb).then((list) =>{

      let dels = [];

      for(let ii = 0; ii < list.length; ii++){

        let item = list[ii];

        if(foreign){
          if(item.fkey == key){
            dels.unshift(ii);
          }
        }else{
          if(item.key == key){
            dels.unshift(ii);
          }
        }
      }

      let ditems = [];

      for(let ii = 0; ii < dels.length; ii++){
        ditems = ditems.concat(list.splice(dels[ii], 1));
      }



      return this.storage.set(listKey, list).then(() => {
        return ditems;
      })

    });
  }




  private getGeneralStringData(tb: Table, key:any):Promise<any> {
    let fullkey = this.getKeyName(tb.name, key);
    return this.storage.get(fullkey).then((val) => {
      if(!val){
        return '';
      }else{
        return val;
      }
    })
  }

  private getGeneralObjData(tb: Table, key:any): Promise<any> {

    return this.getGeneralStringData(tb, key).then((val) => {
      if(!val){
        return null;
      }
      let obj = tb.cls.initFromJObj(JSON.parse(val));
      return obj;
    });
  }

  private getSimpleObjData(tb:Table): Promise<any>{
    let fullkey = this.getKeyName(tb.name, '');
    return this.storage.get(fullkey).then((val) => {
      if(!val){
        return null;
      }

      if(!MUtils.isJsonString(val)){
        return val;
      }

      let obj = tb.cls.initFromJObj(JSON.parse(val));
      return obj;
    })
  }

  private saveSimpleData(tb:Table, data:any): Promise<any>{

    let fullkey = this.getKeyName(tb.name, '');
    let valString = JSON.stringify(data);

    return this.storage.set(fullkey, valString);

  }

  private saveGeneralDataWithoutList(tb: Table, key: any, data: any):Promise<any> {

    let fullkey = this.getKeyName(tb.name, key);
    let valString:String;

    if(MUtils.isString(data)){
      valString = data;
    }else{
      valString = JSON.stringify(data);
    }

    return this.storage.set(fullkey, valString);

  }


  private saveGeneralData(tb: Table, key: any, fkey: any, data: any):Promise<any> {
    let fullkey = this.getKeyName(tb.name, key);
    let valString:String;

    mfuncs.log("save general data, tb:" + tb.name + " key:" + key + " fkey:" + fkey);

    if(MUtils.isString(data)){
      valString = data;
    }else{
      valString = JSON.stringify(data);
    }

    return this.storage.set(fullkey, valString).then((val) =>{
      return this.addToRootList(tb, key, fkey);
    });
  }

  private updateGeneralData(tb: Table, key: any, changeFunc:(x: any)=>Promise<any>):Promise<any>{

    return this.getGeneralObjData(tb, key).then((val) => {

      if(!val){
        throw new MError(ErrorCode.Value_Missing, "no data found tb:" + tb.name + " by key:" + key);
      }

      return changeFunc(val);

    }).then((val) => {
      return this.saveGeneralDataWithoutList(tb, key, val);
    });
  }



  private simpleUpdateGeneralData(tb: Table, key: any, val:any){

    return this.saveGeneralDataWithoutList(tb, key, val);

  }


  private removeGeneralData(tb:Table, key:any){

    let fullkey = this.getKeyNameByTable(tb, key);
    let delVal:any;

    return this.storage.get(fullkey).then((val) => {

      if(MUtils.isString(val) && MUtils.isJsonString(val)){
        delVal = JSON.parse(val);
      }else{
        delVal = val;
      }

      return this.storage.remove(fullkey);

    }).then(() => {
      return this.deleteFromRootListByKey(tb, key, false);

    }).then(() => {
      mfuncs.log('tb name:' + tb.name + ' children count:' + tb.children.length);

      if(tb.children.length < 0){
        return Promise.resolve();
      }

      if(tb.children.length < 0){
        return Promise.resolve();
      }

      return tb.children.reduce((promise, item, currentIndex) =>{

        return promise.then(() =>{

          return this.getTable(item).then((innerTb) => {

            mfuncs.log("del child root...:" + tb.name + " child:" + innerTb.name + " fk:" + innerTb.fkeyName + " key:" + key);

            if(delVal){
              if(!delVal.hasOwnProperty(innerTb.fkeyName)){
                throw new MError(ErrorCode.Value_Missing, "foreign key value missing root:" + tb.name + " child:" + innerTb.name + " fk:" + innerTb.fkeyName);
              }

              let innerKey = delVal[innerTb.fkeyName];

              mfuncs.log("before del from root inner :" + innerKey);

              return this.deleteFromRootListByKey(innerTb, innerKey, true).then((dels) => {

                mfuncs.log("del from roots: " + JSON.stringify(dels));

                for(let ii = 0; ii < dels.length; ii++){
                  return this.removeGeneralData(innerTb, dels[ii].key);
                }
              });

            }else{

              if(innerTb.fkeyName == 'Id'){

                return this.deleteFromRootListByKey(innerTb, key, true).then((dels) => {

                  for(let ii = 0; ii < dels.length; ii++){
                    return this.removeGeneralData(innerTb, dels[ii].key);
                  }
                });

              }
            }
          })
        })
      }, Promise.resolve());
    })
  }





}














































