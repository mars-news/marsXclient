/**
 * Created by meryn on 2017/05/01.
 */
import { Storage } from '@ionic/storage';
import {Injectable} from "@angular/core";
import { NewsItem, NewsSrc} from '../interfaces/news'
import { DataType,Mdb } from './mdb'
import { UserData } from '../interfaces/user-data'
import { ErrorCode, MError } from "../common/merror";
import { Vote } from '../interfaces/comment'
import { mfuncs } from "../common/mfuncs";

const GC_News_Time_Span = 3600 * 12;
const GC_NewsItem_Keep_Count = 20;
const GC_Photo_Time_Span = 3600 * 24 * 10;




@Injectable()
export class MCache{

  static UserTable = "User";
  static NewsItemTable = "NewsItem";
  static NewsSrcTable = "NewsSrc";
  static NewsImageTable = "NewsImage";
  static NewsSrcImageTable = "NewsSrcImage";
  static NewsContentTable = "NewsContent";
  static VoteTable = "Vote";
  static PhotoVersionTable = "PhotoImageVersion";

  static PhotoTable = "Photo";
  static Block_Comment = "BlockComment";
  static Block_User = "BlockUser";


  constructor(public storage: Storage, private mdb:Mdb){

    this.mdb.registerTable(MCache.UserTable, UserData, '', '', DataType.Simple);
    this.mdb.registerTable(MCache.NewsItemTable, NewsItem, '', '', DataType.General, false);
    this.mdb.registerTable(MCache.NewsSrcTable, NewsSrc, '', '', DataType.General);
    this.mdb.registerTable(MCache.NewsImageTable, null, MCache.NewsItemTable, 'Id', DataType.General);
    this.mdb.registerTable(MCache.NewsSrcImageTable, null, MCache.NewsSrcTable, 'Id', DataType.General);
    this.mdb.registerTable(MCache.NewsContentTable, null, MCache.NewsItemTable, 'Id', DataType.General);

    this.mdb.registerTable(MCache.VoteTable, null, MCache.NewsItemTable, 'Id', DataType.General);

    this.mdb.registerTable(MCache.PhotoVersionTable, null, '', '', DataType.General);
    this.mdb.registerTable(MCache.PhotoTable, null, MCache.PhotoVersionTable, '', DataType.General);

    this.mdb.registerTable(MCache.Block_Comment, null, '', '', DataType.General);
    this.mdb.registerTable(MCache.Block_User, null, '', '', DataType.General);


    this.mdb.registerGcDuration(MCache.NewsItemTable, GC_News_Time_Span);
    this.mdb.registerGcKeepCount(MCache.NewsItemTable, GC_NewsItem_Keep_Count);

    this.mdb.registerGcDuration(MCache.PhotoTable, GC_Photo_Time_Span);

    this.mdb.registerGcDuration(MCache.Block_Comment, GC_News_Time_Span * 10);

    let incrementPhotoVerFunc = this.incrPhotoImageVersion;
    this.mdb.registerGcEachKeyFunc(MCache.PhotoTable, function (key:string) {

      let uid:number = +key;

      mfuncs.log("increment photo uid:" + uid);

      if(!uid){
        return incrementPhotoVerFunc(uid); //check;
      }
    });


    this.mdb.afterRegister();

  }


  blockComment(nid: number, pid: number, rid: number, reason: number):Promise<boolean>{
    let key = this.getVoteId(nid, pid, rid, rid > 0);

    return this.mdb.getData(MCache.Block_Comment, key).then((ok) => {
      if(ok){
        return false;
      }else{
        return this.mdb.saveData(MCache.Block_Comment, key, null, reason).then(() => {
          return true;
        })
      }
    });
  }



  checkIfUserOrCommentBlocked(nid:number, pid:number, rid:number, uid:number):Promise<boolean>{
    return this.checkIfCommentBlocked(nid, pid, rid).then((ok) => {
      if(ok){
        return true
      }else{
        return this.checkIfUserBlocked(uid)
      }
    })
  }


  checkIfCommentBlocked(nid: number, pid: number, rid: number):Promise<boolean>{
    let key = this.getVoteId(nid, pid, rid, rid > 0);
    return this.mdb.getData(MCache.Block_Comment, key).then((val) => {
      if(val){
        return true
      }else{
        return false
      }
    })
  }

  blockUser(uid: number, reason: number):Promise<any>{
    return this.mdb.saveData(MCache.Block_User, uid, null, reason)
  }

  checkIfUserBlocked(uid:number):Promise<boolean>{
    return this.mdb.getData(MCache.Block_User, uid).then((val) => {
      if(val){
        return true
      }else{
        return false
      }
    })
  }


  saveUserInfo(uinfo: UserData){
    return this.mdb.saveData(MCache.UserTable, '', '', uinfo);
  }

  getUserInfo():Promise<UserData>{
    return this.mdb.getData(MCache.UserTable, '');
  }

  getSrcItemById(id: number):Promise<NewsSrc>{
    return this.mdb.getData(MCache.NewsSrcTable, id);
  }

  saveSrcItem(src:NewsSrc){
    return this.mdb.saveData(MCache.NewsSrcTable, src.Id, '', src);
  }

  getAllSrcs():Promise<NewsSrc[]>{
    return this.mdb.getAllData(MCache.NewsSrcTable);
  }

  getAllNewsItem(){
    return this.mdb.getAllData(MCache.NewsItemTable);
  }

  updateNewsItem(news:NewsItem):Promise<any>{
    return this.mdb.simpleUpdateData(MCache.NewsItemTable, news.Id, news);
  }

  saveNewsItem(news:NewsItem):Promise<any>{
    return this.mdb.saveData(MCache.NewsItemTable, news.Id, '', news);
  }

  getNewsItemById(id: number):Promise<NewsItem>{
    return this.mdb.getData(MCache.NewsItemTable, id);
  }

  removeNewsItemById(id:number):Promise<any>{
    return this.mdb.removeData(MCache.NewsItemTable, id);
  }


  saveNewsImage(nid: number, url:string, data: string):Promise<any>{
    return this.mdb.saveData(MCache.NewsImageTable, url, nid, data);
  }

  getNewsImage(url:string):Promise<any> {
    return this.mdb.getData(MCache.NewsImageTable, url);
  }

  saveSrcImage(srcId:number, data: string):Promise<any> {
    return this.mdb.saveData(MCache.NewsSrcImageTable, srcId, srcId, data);
  }

  getSrcImage(srcId:number):Promise<any> {
    return this.mdb.getData(MCache.NewsSrcImageTable, srcId);
  }

  savePhoto(uid:number, data: string): Promise<any>{
    return this.mdb.saveData(MCache.PhotoTable, uid, null, data);
  }

  getPhoto(uid:number):Promise<any>{
    return this.mdb.getData(MCache.PhotoTable, uid);
  }

  deletePhoto(uid:number):Promise<any>{
    return this.mdb.removeData(MCache.PhotoTable, uid);
  }


  getContent(url:string){
    return this.mdb.getData(MCache.NewsContentTable, url);
  }

  saveContent(nid:number, url:string, content:string){
    return this.mdb.saveData(MCache.NewsContentTable, url, '', content);
  }


  getVoteId(nid:number, pid:number, rid:number, isReply):string{
    if(isReply){
      return nid + '-' + pid + '-' + rid;
    }else{
      return nid + '-' + pid;
    }
  }


  saveVote(nid:number, pid:number, rid:number, isReply:boolean, voteNum: number):Promise<any>{

    let key = this.getVoteId(nid, pid, rid, isReply);
    //let vote = new Vote(pid, rid, isReply);
    return this.mdb.saveData(MCache.VoteTable, key, nid, voteNum);

  }

  getVote(nid:number, pid:number, rid:number, isReply:boolean):Promise<any>{
    let key = this.getVoteId(nid, pid, rid, isReply);
    return this.mdb.getData(MCache.VoteTable, key).then((val) => {
      return val;
    })
  }


  getPhotoImageVersion(uid: number):Promise<number>{
    return this.mdb.getData(MCache.PhotoVersionTable, uid).then((val) =>{
      if(!val || val == ''){
        return 0;
      }else{
        return +val;
      }
    })
  }




  incrPhotoImageVersion(uid: number):Promise<number>{
    let newVer:number;
    return this.getPhotoImageVersion(uid).then((ver) => {
      if(!ver){
        newVer = 1;
        return this.mdb.saveData(MCache.PhotoVersionTable, uid, null, 1);
      }else{
        newVer = ver+1;
        return this.mdb.saveData(MCache.PhotoVersionTable, uid, null, ver+1);
      }
    }).then(() => {
      return this.deletePhoto(uid);
    }).then((val)=>{
      return newVer;
    });
  }



  gcRecord(tbName: string):Promise<any>{
    mfuncs.log("gc single record start ...");
    return this.mdb.gcOne(tbName).catch((err) => {
      mfuncs.log("gc failed" + err);
    })

  }

  gcRecords():Promise<any>{
    mfuncs.log("gc records start ...");

    return this.mdb.gc().catch((err) => {
      mfuncs.log("gc failed" + err);
    })
  }


}



















