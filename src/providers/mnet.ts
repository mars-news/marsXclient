/**
 * Created by meryn on 2017/06/08.
 */

import { Injectable } from '@angular/core'
import {Http, Headers, RequestOptions} from '@angular/http'
import 'rxjs/add/operator/map'

import { MError, ErrorCode } from '../common/merror';
import { mfuncs } from "../common/mfuncs";

@Injectable()
export class MNet {

  constructor(public http: Http){

  }

  sendGet(url:string, params: any, rspCheckList: string[]):Promise<any>{

    let parStr = "?";

    for(let key in params){

      let value = params[key];
      parStr += key + "=" + value + "&";

    }


    return new Promise((resolve, reject) => {

      mfuncs.log("http :" + url + parStr);

      this.http.get( url + parStr)
        .map(res => res.json())
        .subscribe(data => {

          if(!data.hasOwnProperty('Status')){
            reject(new MError(ErrorCode.Response_Status_Missing, "status misssing:"));
            return
          }

          if(data['Status'] != 0){
            //if(data['Message'] == '')
            if(data['Msg'].indexOf('auth')>=0){
            }

            reject(new MError(ErrorCode.Response_Status_Value_Wrong, "status value wrong"))
            return
          }


          if(!data.hasOwnProperty('Body')){
            reject(new MError(ErrorCode.Response_Body_Missing, "body misssing:"));
            return
          }

          let body = data['Body'];

          for(let item of rspCheckList){

            if(!body.hasOwnProperty(item)){
              reject(new MError(ErrorCode.Response_Body_Field_Missing, "body field missing:" + item));
              return
            }
          }

          resolve(body);

        }, (err) =>{
          reject(new MError(ErrorCode.Http_Request_Failed, "my http error:" + err.toString()));
        });
    });
  }


  serializePostData( data ) {
    let buffer = [];
    // Serialize each key in the object.
    for ( let name in data ) {
      if ( ! data.hasOwnProperty( name ) ) {
        continue;
      }
      let value = data[ name ];
      buffer.push(
        encodeURIComponent( name ) +
        "=" +
        encodeURIComponent( ( value == null ) ? "" : value )
      );
    }
    // Serialize the buffer and clean it up for transportation.
    let source = buffer
      .join( "&" )
      .replace( /%20/g, "+" )
    ;
    return( source );
  }


  send(url:string, params: any, rspCheckList: string[]):Promise<any>{

    return new Promise((resolve, reject) => {

      let postParams = this.serializePostData(params);

      mfuncs.log("http post :" + url + " params:" + postParams);

      let headers = new Headers(
        {
          //'Content-Type' : 'application/json'
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        });
      let options = new RequestOptions({ headers: headers });

      this.http.post( url, postParams, options)
        .map(res => res.json())
        .subscribe(data => {

          if(!data.hasOwnProperty('Status')){
            reject(new MError(ErrorCode.Response_Status_Missing, "status misssing:"));
            return
          }

          if(data['Status'] != 0){
            //if(data['Message'] == '')
            if(data['Msg'].indexOf('auth')>=0){
            }

            reject(new MError(ErrorCode.Response_Status_Value_Wrong, "status value wrong"))
            return
          }


          if(!data.hasOwnProperty('Body')){
            reject(new MError(ErrorCode.Response_Body_Missing, "body misssing:"));
            return
          }

          let body = data['Body'];

          for(let item of rspCheckList){

            if(!body.hasOwnProperty(item)){
              reject(new MError(ErrorCode.Response_Body_Field_Missing, "body field missing:" + item));
              return
            }
          }

          resolve(body);

        }, (err) =>{
          reject(new MError(ErrorCode.Http_Request_Failed, "my http error:" + err.toString()));
        });
    });
  }


  loadFile(path:string):Promise<string>{

    return new Promise((resolve, reject) => {

      this.http.get(path).map(res => res).subscribe(data => {
        resolve(data["_body"])
      });
    })
  }

}














