/**
 * Created by meryn on 2017/04/30.
 */

import { Injectable } from '@angular/core'
import { Platform } from 'ionic-angular'


@Injectable()
export class MUrls{

  //staticBaseUrl = 'http://127.0.0.1:9801';
  //endBaseUrl = 'http://127.0.0.1:9805';

  staticBaseUrl = 'https://static.marsladder.com:9601';
  endBaseUrl = 'https://news.marsladder.com:9605';

  registerProxy = '/user/register';
  registerUrl = this.endBaseUrl + this.registerProxy;

  loginProxy = '/user/login';
  loginUrl = this.endBaseUrl + this.loginProxy;

  newsRefreshProxy = '/news/refresh';
  newsRefreshUrl = this.endBaseUrl + this.newsRefreshProxy;

  newsRateProxy = '/news/rate';
  newsRateUrl = this.endBaseUrl + this.newsRateProxy;

  commentPublishProxy = '/comment/publish';
  commentPublishUrl = this.endBaseUrl + this.commentPublishProxy;

  commentRefreshProxy = '/comment/refresh';
  commentRefreshUrl = this.endBaseUrl + this.commentRefreshProxy;

  reportProxy = '/report';
  reportUrl = this.endBaseUrl + this.reportProxy;

  replyRefreshProxy = '/reply/refresh';
  replyRefreshUrl = this.endBaseUrl + this.replyRefreshProxy;

  voteProxy = '/vote';
  voteUrl = this.endBaseUrl + this.voteProxy;

  newsStaticProxy = '/news-static';
  newsStaticUrl = this.staticBaseUrl + this.newsStaticProxy;

  srcImageProxy = '/source-image';
  srcImageUrl = this.staticBaseUrl + this.srcImageProxy;

  photoStaticProxy = '/photos';
  photoUrl = this.staticBaseUrl + this.photoStaticProxy;

  uploadPhotoProxy = '/user/photo/upload';
  uploadPhotoUrl = this.endBaseUrl + this.uploadPhotoProxy;

  userInfoUpdateProxy = '/user/info/update';
  userInfoUpdateUrl = this.endBaseUrl + this.userInfoUpdateProxy;




  constructor(public platform: Platform){
  }

  getRegisterUrl(): string{
    if (this.platform.is('core')) {
      return this.registerProxy;
    } else {
      return this.registerUrl;
    }
  }

  getLoginUrl(): string{
    if (this.platform.is('core')) {
      return this.loginProxy;
    } else {
      return this.loginUrl;
    }
  }

  getNewsRefreshUrl(): string{
    if (this.platform.is('core')) {
      return this.newsRefreshProxy;
    } else {
      return this.newsRefreshUrl;
    }
  }

  getNewsRateUrl(): string{
    if (this.platform.is('core')) {
      return this.newsRateProxy;
    } else {
      return this.newsRateUrl;
    }
  }

  getCommentPublishUrl(): string{
    if (this.platform.is('core')) {
      return this.commentPublishProxy;
    } else {
      return this.commentPublishUrl;
    }
  }

  getCommentRefreshUrl(): string{
    if (this.platform.is('core')) {
      return this.commentRefreshProxy;
    } else {
      return this.commentRefreshUrl;
    }
  }

  getReplyRefreshUrl(): string{
    if (this.platform.is('core')) {
      return this.replyRefreshProxy;
    } else {
      return this.replyRefreshUrl;
    }
  }

  getReportUrl(): string{
    if (this.platform.is('core')) {
      return this.reportProxy;
    } else {
      return this.reportUrl;
    }
  }

  getVoteUrl(): string{
    if (this.platform.is('core')) {
      return this.voteProxy;
    } else {
      return this.voteUrl;
    }
  }


  getRefreshUrl(): string{
    if (this.platform.is('core')) {
      return this.newsRefreshProxy;
    } else {
      return this.newsRefreshUrl;
    }
  }

  getNewsStaticUrl(): string{
    if (this.platform.is('core')){
      return this.newsStaticProxy;
    }else{
      return this.newsStaticUrl;
    }
  }


  getSrcImageUrl(): string{
    if (this.platform.is('core')){
      return this.srcImageProxy;
    }else{
      return this.srcImageUrl;
    }
  }


  getPhotoImageUrl(): string{
    if (this.platform.is('core')){
      return this.photoStaticProxy;
    }else{
      return this.photoUrl;
    }
  }

  getUserInfoUpdateUrl(): string{
    if (this.platform.is('core')){
      return this.userInfoUpdateProxy;
    }else{
      return this.userInfoUpdateUrl;
    }
  }


  getUploadPhotoUrl(): string{
    if (this.platform.is('core')){
      return this.uploadPhotoProxy;
    }else{
      return this.uploadPhotoUrl;
    }
  }




}


























