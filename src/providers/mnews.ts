/**
 * Created by meryn on 2017/04/18.
 */

import { Injectable } from '@angular/core'
import { MUrls } from './murl'
import { MNet } from './mnet'
import { MCache } from './mcache'

import { MError, ErrorCode } from '../common/merror'

import {Http, ResponseContentType} from '@angular/http'
import 'rxjs/add/operator/map'

import {NewsItem, NewsSrc} from "../interfaces/news";
import * as util from "util";
import { mfuncs } from "../common/mfuncs";

export enum MImageType{

  SourceImage,
  ContentImage,
  PhotoImage,
}



@Injectable()
export class MNews{

  isInitingNews = false;

  newsAll = new Map<number, NewsItem[]>();
  srcsAll = new Map<number, NewsSrc>();

  refreshThreshold = 2;

  constructor(public http: Http,
              public mnet: MNet,
              public cache:MCache,
              private murls: MUrls){
  }

  getSrcById(srcId:number):NewsSrc{
    if(this.srcsAll.has(srcId)){
      return this.srcsAll[srcId];
    }else{
      return null;
    }
  }


  hasRemainingNews(){
    return this.newsAll.size > 0 && this.srcsAll.size > 0;
  }

  getRemainingNewsLength():number{
    return this.newsAll.size;
  }

  hasRemainingNewsLengthByCat(cat:number):boolean{

      let news = this.newsAll.get(cat);

      return news && news.length >0;

  }


  InitNews(){

    mfuncs.log("init news here !!!!!!!!!!!!!!");

    return this.cache.gcRecord(MCache.NewsItemTable).then(() => {

      return this.cache.getAllNewsItem()
        .then((list_) => {
          let list = <NewsItem[]>list_;
          for(let ii = 0; ii < list.length; ii++){
            let item = <NewsItem>list[ii];
            let catid = item.RefreshCat;

            if(!this.newsAll.has(catid)){
              this.newsAll.set(catid, [item])
            }else{
              this.newsAll.get(catid).push(item)
            }

            mfuncs.log("init cat:" + catid + " len:" + this.newsAll.get(catid).length);

          }

          return list.length
        })

    });

  }


  InitSrcs(){

    return this.cache.getAllSrcs()
        .then((val_) => {

        let val = <NewsSrc[]>val_;

        for(let ii = 0; ii < val.length; ii++){

          let item = val[ii];
          this.srcsAll.set(item.Id, item)
        }

        return val.length
        })

  }

  getContent(nid:number, url:string){

    return this.cache.getContent(url)
      .then((content) => {
        if(!content){
          return this.downloadContent(url).then((image)=>{
            //mfuncs.log("image downloaded:", data)
            this.cache.saveContent(nid, url, <string>image);
            return image;
          })
        }else{
          return content;
        }

      })
  }


  getAllNewsByCat(catid: number):Promise<NewsItem[]>{

    let news = this.newsAll.get(catid);

    mfuncs.log("get news cat:" + catid);

    if(news && news.length >= this.refreshThreshold){
      return Promise.resolve(news);
    }

    return this.refresh(catid)
  }

  addNewsToCat(catid: number, newNews:NewsItem){

    if(!this.newsAll.has(catid)){
      this.newsAll.set(catid, []);
    }

    let news = this.newsAll.get(catid);

    for(let ii =0; ii < news.length; ii++){

      if(news[ii].Id == newNews.Id){

        return Promise.resolve();

      }
    }

    news.unshift(newNews);

    return this.cache.saveNewsItem(newNews)

  }



  refresh(catid:number): Promise<NewsItem[]>{
    mfuncs.log("refresh" + catid);

    let params = {};
    if(catid > 0){
      params = {cat: catid-1}
    }

    return this.mnet.send(this.murls.getRefreshUrl(), params, ["News", "Srcs"]).then((rsp) => {

        let news = rsp.News;

        return news.reduce((promise, val, currentIndex) =>{

          return promise.then(() =>{

            let newsItem = NewsItem.initFromJObj(val);
            newsItem.RefreshCat = catid;
            return this.addNewsToCat(catid, newsItem)
          })

        }, Promise.resolve()).then(()=>{

          let srcs = rsp.Srcs;

          return srcs.reduce((promise, val, currentIndex) => {

            return promise.then(() => {
              let src = val;
              let srcId = src.Id;

              if (this.srcsAll.has(srcId)) {
                return;
              }

              let srcItem =  NewsSrc.initFromJObj(src);
              this.srcsAll.set(srcId, srcItem);

              mfuncs.log("src get image 0:" + srcId);
              return this.cache.saveSrcItem(srcItem);

            });

          }, Promise.resolve())

        })

      }).then(() => {

        mfuncs.log("refresh last line");

        return this.newsAll.get(catid);

      });
  }


  downloadSrcImage(url:string){
    return this.downloadImage(MImageType.SourceImage, url)
  }

  getSrcImage(srcId: number):Promise<string>{

    mfuncs.log("src get image:" + srcId);

    return this.cache.getSrcImage(srcId)
      .then((imgCache) => {

        if (imgCache) {
          return imgCache;
        } else {
          mfuncs.log("src get image:" + srcId);
          let src = this.srcsAll.get(srcId);
          if(src) {

            return this.downloadSrcImage(src.PicPath).then((imageData) =>{
              return this.cache.saveSrcImage(src.Id, imageData).then((saveTime) => {
                return imageData;
              });
            })
          }else{
            throw new MError(ErrorCode.Src_Missing, "src is not existed, " + srcId)
          }
        }

      });
  }

  getPhotoImage(uid: number, ver: number):Promise<any>{   //check

    let fromCache = false;

    return this.cache.getPhoto(uid).then((imgCache) => {

      if(imgCache){
        mfuncs.log("photo has cache");

        fromCache = true;

        return imgCache;
      }else {
        mfuncs.log("photo has no cache");
        return this.downloadImage(MImageType.PhotoImage, util.format('%d.jpg?version=%d', uid, ver));
      }
    }).then((imgData) => {

      if(!fromCache){
        this.cache.savePhoto(uid, <string>imgData);
      }

      return imgData;
    });
  }


  getContentImage(nid:number, url:string){

    return this.cache.getNewsImage(url)
      .then((imgCache) => {

        if(imgCache){
          return imgCache;
        }else {

          return this.downloadContentImage(nid, url)

        }

      });

  }

  downloadContentImage(nid:number, url:string){
    return this.downloadImage(MImageType.ContentImage, url)
      .then((data) =>{
        this.cache.saveNewsImage(nid, url, <string>data);
        return data;
      })
  }


  downloadImage(imageType: MImageType , url:string):Promise<string>{
    return new Promise((resolve, reject) =>{

      var imageStaticUrl : string = "";

      if(imageType == MImageType.SourceImage){
        imageStaticUrl = this.murls.getSrcImageUrl();
      }else if (imageType == MImageType.ContentImage){
        imageStaticUrl = this.murls.getNewsStaticUrl();
      }else if (imageType == MImageType.PhotoImage){
        imageStaticUrl = this.murls.getPhotoImageUrl();
      }else{
        throw new MError(ErrorCode.Type_Missing, "image type is not exists:" + imageType);
      }

      mfuncs.log("image download url:" + imageStaticUrl + "/" + url);
      this.http.get(imageStaticUrl + "/" + url, {responseType: ResponseContentType.Blob})
        .map(rsp => rsp )
        .subscribe(rsp => {
            let bb = rsp.blob()
            let reader = new FileReader();
            reader.readAsDataURL(bb);
            reader.onloadend = function() {
              let base64data = reader.result;
              resolve(base64data)
            }
          },
          (err) => {
            reject(err)
          }
        );

    })
  }

  downloadContent(contentUrl){
    return new Promise((resolve, reject) => {
      mfuncs.log("content:" + this.murls.getNewsStaticUrl() + "/" + contentUrl)
      this.http.get(this.murls.getNewsStaticUrl() + "/" + contentUrl)
        .map(res => res)
        .subscribe(data => {
          let ret = data.text();
          //mfuncs.log("content:" + ret);
          resolve(ret);
        }, (err) =>{
          mfuncs.log("download content failed" + JSON.stringify(err));
          reject(reject);
        });
    })
  }

  updateCommentCnt(nid:number, commentCnt:number): Promise<any>{

    for(let key of Array.from(this.newsAll.entries())){
      let list = key[1];
      for(let ii = 0; ii < list.length; ii++){
        let news = list[ii];
        if(news.Id == nid){
          news.CommentCnt = commentCnt;
        }
      }
    }

    return this.cache.getNewsItemById(nid).then((item) => {
      item.CommentCnt = commentCnt;
      return this.cache.updateNewsItem(item)

    })


  }





}






