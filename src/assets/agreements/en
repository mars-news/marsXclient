LiteNews 利用規約
目的
この LiteNews 利用規約（以下「本規約」）は、スマートニュース株式会社（以下「当社」）が「LiteNews」の名称にて提供するアプリケーション（以下「本サービス」）の利用条件を定めるものです。本サービスの利用者（以下「ユーザ」）は、本規約を遵守しなければなりません。
利用規約の変更
当社は、ユーザの事前の了承を得ることなく、本規約を改定することができ、ユーザはそれを承諾するものとします。改定後の利用規約は、本サービスに掲示された時点より効力を発するものとします。
個人情報の利用
当社は、本サービスの提供にあたり、別途定めるプライバシーポリシーに則り、ユーザの個人情報を利用するものとします。
著作権
本サービスに関する著作権および知的財産権は、当社に帰属します。
本サービスを通じて当社が提供する情報に関する著作権および知的財産権は、かかる情報に関して正当な権限を有する第三者に帰属します。
禁止事項
ユーザは、本サービスの利用にあたり、以下の行為を行ってはならないこととします。
当社または第三者の著作権、その他の知的財産権または財産的利益を侵害する行為、またはそのおそれのある行為
法令、公序良俗に反する行為、またはそのおそれのある行為
他のユーザまたは第三者に不利益を与える行為、またはそのおそれのある行為
本サービスの運用を妨げる行為、またはそのおそれのある行為
当社または本サービスの信用を毀損する行為、またはそのおそれのある行為
本サービスに含まれるソフトウェア情報および著作物について、リバースエンジニアリング、逆コンパイル、または逆アセンブルする行為
当社のネットワークおよび本サービスを提供するにあたり使用しているインターネットサーバ（以下「対象設備」）に不正にアクセスし、または蓄積された情報を不正に書き換えもしくは消去する行為
対象設備にウイルス等の有害なコンピュータプログラム等を送信する行為
対象設備に必要以上の負荷をかける行為
その他、当社が不適切と判断する行為
サービスの変更等
当社は、本サービスの一部または全部を、ユーザへ事前に通知することなく、いつでも任意の理由で変更、中断、停止または終了することができるものとします。
免責事項
本サービスは、ユーザが自身の判断と責任において、以下を承諾の上で利用するものとし、ユーザに生じたいかなる損失または損害についても、当社は一切の責任を負わないものとします。
当社は、理由の如何を問わず、本サービスの利用または本サービスを利用できなかったことに関してユーザに生じたいかなる損害についても、一切の責任を負いません。
当社は、本サービスの動作および使用機器への適合性について、一切保証しません。
当社は、本サービスの完全性、正確性、適法性、有用性、および本サービスの利用により取得された情報の正確性、妥当性、適法性、有用性、その他一切の事項について保証しません。
当社は、アクセス過多、その他予期せぬ原因による本サービスの表示速度の低下や障害等によって生じたいかなる損害についても、一切の責任を負いません。
当社は、本サービスの変更、中断、停止または終了によって生じたいかなる損害についても、一切の責任を負いません。
準拠法
本規約の成立、効力、履行および解決に関しては、日本国の法令が適用されるものとします。