import { AppState } from './app.global';
import { Component, ViewChild } from '@angular/core';
import {Platform, Config, ToastController, Nav, NavController, ViewController, App} from 'ionic-angular';

import { NgZone } from '@angular/core';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TranslateService } from '@ngx-translate/core';

import { MDef } from '../common/mdefs'
import { MUser } from "../providers/muser";
import { MCache } from '../providers/mcache'
import { MWords } from '../providers/mwords'
import { MError, ErrorCode } from '../common/merror'
import { Storage } from '@ionic/storage';
import { mfuncs } from '../common/mfuncs'


@Component({
  selector: 'app-page',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;


  Version = 1.1;

  rootPage:string;// = "HomePage";
  private backPressed: boolean = false;

  constructor(private zone: NgZone,
              private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              public global: AppState,
              private translate: TranslateService,
              private config:Config,
              private storage: Storage,

              private app: App,
              private toastCtrl: ToastController,
              private muser: MUser,
              private mcache: MCache,
              private mwords: MWords,
              ) {

    this.initializeApp();
    this.initTranslate();
    /*this.app.viewWillEnter.subscribe(viewCtrl => {
      mfuncs.log('app Entering new view');
      mfuncs.log(viewCtrl)
    })*/

  }



  initializeApp() {

    let ver: string;

    this.storage.ready().then(() => {

      return this.storage.get(MDef.App_Version_Key);

    }).then((val) => {

      ver = val;
      return this.storage.get(MDef.App_User_Key);

    }).then((val) => {

      if(ver && val){
        mfuncs.log("normal start version:" + ver);
        return this.noramlStart();
      }else{
        mfuncs.log("first open app");
        return this.firstStart();
      }
    }).catch((err) => {

      mfuncs.log("app startup failed" + err)

    });


    this.platform.ready().then(() => {

      //this.global.set('theme', "theme-dark");

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.mwords.loadWordsFile();  //todo

      this.mcache.gcRecord(MCache.PhotoTable);

      this.platform.resume.subscribe(() => {
        this.muser.userLogin()
      });

      this.platform.registerBackButtonAction(() => {
        let nav: NavController = this.app.getActiveNav();
        let view: ViewController = nav.getActive();

        if (view && view.enableBack()) {
          view.dismiss();
          return
        }

        if (!this.backPressed){
          this.backPressed = true;
          let toast = this.toastCtrl.create({
            message: 'press again',
            duration: 1500,
            position: 'middle'
          });
          toast.present();

          setTimeout(() => this.backPressed = false, 2000 );
          return;

        }
        this.platform.exitApp()

      });

    });


  }



  initTranslate(){

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('ja'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });

  }



  noramlStart():Promise<any>{

    return this.muser.userLogin().then((val) => {
      mfuncs.log("login successed");
      this.rootPage = "TabsPage";

      return true
    })

  }


  firstStart():Promise<boolean>{

    return this.storage.set(MDef.App_Version_Key, this.Version).then((val) => {
      if(!val){
        throw new MError(ErrorCode.Storage_Set_Failed, "set version failed" + this.Version);
      }

      this.rootPage = "InterestSelectionPage";

      return true;
    });


  }


}
