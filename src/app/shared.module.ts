/**
 * Created by meryn on 2017/06/23.
 */

import { Components, Directives, Pipes } from './app.imports'
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({

  declarations: [
    Pipes,
    Directives,
    Components,
  ],
  imports: [
    IonicModule,
    TranslateModule
  ],
  exports: [
    Pipes,
    Components,
    TranslateModule
  ]
})
export class SharedModule{ }






