/**
 * Created by meryn on 2017/06/23.
 */
import { AppState } from './app.global';

//Pages

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


var firebaseConfig = {
  apiKey: "AIzaSyCNLlC3Gig-2fjTj5BrWfxByArzEbCkp3U",
  authDomain: "mynews-afff5.firebaseapp.com",
  databaseURL: "https://mynews-afff5.firebaseio.com",
  projectId: "mynews-afff5",
  storageBucket: "mynews-afff5.appspot.com",
  messagingSenderId: "24243431764"
};

// Ionic native providers
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Transfer } from '@ionic-native/transfer'
import { Camera } from '@ionic-native/camera'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path'
import { AngularFireModule } from 'angularfire2'

// Providers
import { MComment } from '../providers/mcomment'
import { MCache } from '../providers/mcache'
import { MNews } from '../providers/mnews'
import { MUser } from '../providers/muser'
import { Mdb } from '../providers/mdb'
import { MUrls } from '../providers/murl'
import { MNet } from '../providers/mnet'

// Modules
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser'

// Pipes
import { TimePast } from '../pipes/time-past'
import { UNameExtractPipe } from '../pipes/uname-extract'
import { LongTextCut } from '../pipes/long-text-cut'
import { TimePastSplit } from '../pipes/time-past-split'


// Components
import { MCommentItem } from '../components/m-comment/m-comment-item'
import { MImg } from '../components/m-img/m-img'
import { MSlide } from '../components/m-slide/m-slide'
import { MNewslistItem } from '../components/m-newslist-item/m-newslist-item'
import {MWords} from "../providers/mwords";



export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export const Modules = [

  BrowserModule,
  HttpModule,
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }
  }),
  IonicStorageModule.forRoot(),
  AngularFireModule.initializeApp(firebaseConfig)

];


export const Pages = [


];


export const Pipes = [
  TimePast,
  UNameExtractPipe,
  LongTextCut,
  TimePastSplit
];



export const Components = [

  MCommentItem,
  MImg,
  MSlide,
  MNewslistItem,

];


export const Providers = [

  AppState,
  StatusBar,
  SplashScreen,
  Transfer,
  Camera,
  File,
  FilePath,
  InAppBrowser,

  MWords,
  MNet,
  MCache,
  MComment,
  MNews,
  MUser,
  Mdb,
  MUrls,
];

export const Directives = [


];




















