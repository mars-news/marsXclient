import { SharedModule } from './shared.module'
import { Modules, Pages, Providers } from './app.imports'

import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';





@NgModule({
  declarations: [
    MyApp,
    Pages,
  ],
  imports: [
    Modules,
    IonicModule.forRoot(MyApp),
    SharedModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Pages,
  ],
  providers: [
    Providers,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
