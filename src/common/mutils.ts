import * as util from "util";
/**
 * Created by meryn on 2017/06/13.
 */




export class MUtils {


  static twoDateDiffInSec(date1: Date, date2: Date):number {
    let diffMilli = date1.getTime() - date2.getTime();
    return diffMilli/1000;
  }

  static date2str(date: Date): string{

    return util.format("%s-%s-%s %s:%s:%s", date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());

  }


  static isString(val){
    if(val && typeof val == 'object'){
      return val.constructor == String;
    }
    return typeof val == 'string';
  }


  static  isJsonString(jstr: string):boolean{

    if(!jstr.startsWith("{")){
      return false;
    }

    try {
      JSON.parse(jstr);
    } catch (e) {
      return false;
    }
    return true;
  }


}














