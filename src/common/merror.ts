/**
 * Created by meryn on 2017/06/08.
 */


export enum ErrorCode {

  Http_Request_Failed,

  Response_Status_Missing,
  Response_Status_Value_Wrong,
  Response_Body_Missing,
  Response_Body_Field_Missing,
  Response_Field_Missing,
  Response_Field_Wrong,

  Response_Reg_Id_Wrong,
  Response_Token_Missing,
  Response_Name_Missing,

  Load_User_From_Cache_Failed,

  Login_Failed,

  User_Info_From_Cache,

  Storage_Set_Failed,

  Refresh_Empty,

  Src_Missing,

  Type_Missing,

  Table_Missing,

  Value_Missing,

  Value_Repeated,

}


export enum RspErrorCode{

  None,

  ErrorCode_Not_Meet,
  ErrorCode_Suggest_End,
  ErrorCode_Suggest_Failed,

  ErrorCode_CommentCnt_Overflow,
  ErrorCode_Comment_Publish_Failed,

  ErrorCode_Comment_Refresh_End,

  ErrorCode_Slice_Got_Empty,

  ErrorCode_Default_Empty,

  ErrorCode_Used_Up,

  ErrorCode_User_Blocked,

}


export class MError extends Error{

  code: ErrorCode;

  constructor(code:ErrorCode, m: string){
    super(m);
    Object.setPrototypeOf(this, MError.prototype);

    this.code = code;
  }

  getCode(): ErrorCode{
    return this.code;
  }

}











