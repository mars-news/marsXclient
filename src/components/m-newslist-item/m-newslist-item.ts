/**
 * Created by meryn on 2017/04/27.
 */

import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core'
import {NewsItem} from "../../interfaces/news";
import {MImg} from "../m-img/m-img";
import { mfuncs } from "../../common/mfuncs"
import has = Reflect.has;

@Component({
  selector: 'm-newslist-item',
  templateUrl: 'm-newslist-item.html'
})
export class MNewslistItem{
  @Input("news") news: NewsItem;
  @ViewChild("rightImg") rightImg: MImg;
  @Output('newsClick') newsClick = new EventEmitter<NewsItem>();

  hasRightImage: boolean = true;

  ngAfterViewInit() {

    if(this.rightImg){

      mfuncs.log("news item" + this.news.SourceId);

      this.rightImg.registerShowSubject((hasData:boolean) => {

        this.hasRightImage = hasData

      })

    }


  }

  onNewsClick(event){

    this.newsClick.emit(this.news)
  }


}





















