/**
 * Created by meryn on 2017/06/23.
 */

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {MNewslistItem} from "./m-newslist-item";
import {SharedModule} from "../../app/shared.module";

@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(MNewslistItem),
  ],
  exports: [
    MNewslistItem
  ]
})

export class MNewslisttemModule { }
