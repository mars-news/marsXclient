/**
 * Created by meryn on 2017/06/23.
 */

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {MCommentItem} from "./m-comment-item";
import {SharedModule} from "../../app/shared.module";

@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(MCommentItem),
  ],
  exports: [
    MCommentItem
  ]
})

export class MCommentItemModule { }
