/**
 * Created by meryn on 2017/06/07.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core'
import { Comment } from '../../interfaces/comment'
import { MCache } from "../../providers/mcache";
import { mfuncs } from "../../common/mfuncs"

@Component({

  selector: 'm-comment-item',
  templateUrl: 'm-comment-item.html'
})
export class MCommentItem{

  @Input('comment') comment : Comment;
  @Input('hideVote') hideVote: boolean;
  @Input('hideReport') hideReport: boolean;
  @Input('hideReplyCount') hideReplyCount: boolean;

  @Output("commentClick") slideClick = new EventEmitter<number>();
  @Output('voteClick') voteClick = new EventEmitter<Comment>();
  @Output('reportClick') reportClick = new EventEmitter<Comment>();

  photover:number;

  constructor(private mcache:MCache){

  }

  ngOnInit() {

    //let uExtInfo = UserNameExtInfo.GetUserNameExtInfoFrom(this.comment.Name);

    this.mcache.getPhotoImageVersion(this.comment.Uid).then((ver) => {   //check

      this.photover = ver;

      mfuncs.log('show comment:' + this.comment.Id + " " + this.comment.Content + " name:" + this.comment.Name + " uid:" + this.comment.Uid + " ver:" + this.photover);
    });




  }

  onCommentClick(event){
    mfuncs.log("click comment item" + this.comment.Id);
    this.slideClick.emit(this.comment.Id)
  }

  onVoteClick(event){

    mfuncs.log("click vote ");
    this.voteClick.emit(this.comment)
  }

  onReportClick(event){

    mfuncs.log("click report ");
    this.reportClick.emit(this.comment)
  }


}





















