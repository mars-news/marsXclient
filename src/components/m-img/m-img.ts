/**
 * Created by meryn on 2017/04/27.
 */

import {Component, Input, Output} from '@angular/core'
import { MNews } from '../../providers/mnews'
import { MUser } from '../../providers/muser'
import { MError, ErrorCode } from "../../common/merror";
import {Subject} from "rxjs/Subject";
import {UserNameExtInfo} from "../../interfaces/user-data";
import { mfuncs } from "../../common/mfuncs"

@Component({
  selector: 'm-img',
  templateUrl: 'm-img.html'
})
export class MImg{

  private DefaultUserPhotoPath = "assets/img/profile.png";

  @Input("kind") kind:string;
  @Input("height") height:number;
  @Input("src") src: string;
  @Input("srcId") srcId: number;
  @Input("nid") nid: number;
  @Input("uid") uid: number;
  @Input("ver") ver: number;
  @Input("uname") uname: string;

  srcData: string;


  updateImageSubject = new Subject();
  showSubject = new Subject();


  constructor(public newsData:MNews, private muser:MUser){

  }

  ngOnInit() {

    this.showImage();

    this.updateImageSubject.subscribe((val) => {

      mfuncs.log("refresh image");
      this.showImage();

    });
  }


  registerShowSubject(callback:any){

    let showCallback = callback;
    this.showSubject.subscribe((val) => {

      mfuncs.log("show image subject" + this.src);
      showCallback(val);

    });

  }


  showImage(){
    Promise.resolve("").then(() => {

      if(!this.kind){
        throw new MError(ErrorCode.Type_Missing, "kind is null src:" + this.src + " srcId:" + this.srcId + " nid" + this.nid + " uid:" + this.uid);
      }

      switch (this.kind) {
        case "srcImage":
          mfuncs.log("start show src image " + this.srcId);
          return this.newsData.getSrcImage(this.srcId);
        case "contentImage":
          mfuncs.log("start show content image nid:" + this.nid + " src:" + this.src);
          return this.newsData.getContentImage(this.nid, this.src);
        case "photo":
          mfuncs.log("start show photo image nid:" + this.uid + " ver:" + this.ver + " uname:" + this.uname);

          let extInfo = UserNameExtInfo.GetUserNameExtInfoFrom(this.uname);

          if(extInfo && extInfo.hasPhoto){
            mfuncs.log("start show photo by uid");
            return this.newsData.getPhotoImage(this.uid, this.ver);
          }else{
            mfuncs.log("start show photo by default no ext info:" + this.uname);
            return this.DefaultUserPhotoPath;
          }

        case "":
          throw new MError(ErrorCode.Type_Missing, "kind is missing" + this.kind);
      }
    }).then((imageData) => {

      this.srcData = <string>(imageData);

      if(this.srcData){
        this.showSubject.next(true);
      }else {
        this.showSubject.next(false);
      }

    }).catch((err) => {

      mfuncs.log("show image:" + this.kind + " err:"+ JSON.stringify(err));
      this.showSubject.next(false);
    })
  }


}





















