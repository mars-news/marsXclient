/**
 * Created by meryn on 2017/04/27.
 */

import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core'
import {Slides, Events} from "ionic-angular";
import { mfuncs } from "../../common/mfuncs"

@Component({
  selector: 'm-slide',
  templateUrl: 'm-slide.html'
})
export class MSlide{
  @ViewChild(Slides) obj: Slides;

  @Input("slides") slides:string[] = [];
  @Input("pageNumber") pageNumber: number = 4;
  @Output("slideClick") slideClick = new EventEmitter<number>();


  selectedIndex: number = 0;

  constructor(){

  }

  ngOnInit(){

    this.obj.loop = false;
    this.obj.autoplay = false;
    this.obj.initialSlide = 0;
    this.obj.pager = false;
    this.obj.slidesPerView = this.pageNumber;
    this.obj.paginationHide = true;
    this.obj.paginationClickable = true;

  }

  onClick(index){
    mfuncs.log("click slide" + index);

    this.selectedIndex = index;


    this.slideClick.emit(index)
  }

}
