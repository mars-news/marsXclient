/**
 * Created by meryn on 2017/06/15.
 */
import { Pipe, PipeTransform } from '@angular/core'

const Max_Text_Lenggh = 40;



@Pipe({name: 'longTextCut'})
export  class LongTextCut implements PipeTransform{

  transform(value: any, arg) : any{
    let oriText = value;
    let newText = '';

    if(oriText.length > Max_Text_Lenggh){

      newText = oriText.substr(0, Max_Text_Lenggh) + "…";

    }else{

      newText = oriText;
    }

    return newText;

  }



}





