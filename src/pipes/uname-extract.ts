/**
 * Created by meryn on 2017/06/15.
 */
import { Pipe, PipeTransform } from '@angular/core'
import { UserNameExtInfo } from '../interfaces/user-data'
import { mfuncs } from "../common/mfuncs";

@Pipe({name: 'unameExtract'})
export  class UNameExtractPipe implements PipeTransform{

  transform(value: any, arg) : any{
    let extInfo = UserNameExtInfo.GetUserNameExtInfoFrom(value);
    mfuncs.log("pipe ext:" + JSON.stringify(extInfo));
    if(!extInfo){
      return value;
    }
    return extInfo.name;
  }



}





