/**
 * Created by meryn on 2017/06/15.
 */
import { Pipe, PipeTransform } from '@angular/core'


@Pipe({name: 'timePastSplit'})
export  class TimePastSplit implements PipeTransform{


  transform(value: any, arg) : any{

    let idx = value.indexOf(" ");

    if(arg == 'num'){
      let numStr = value.substr(0, idx);
      return numStr;
    }else{
      let textStr = value.substring(idx + 1);
      return textStr;
    }
  }
}





