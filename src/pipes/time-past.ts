/**
 * Created by meryn on 2017/06/15.
 */
import { Pipe, PipeTransform } from '@angular/core'

const day = 1000 * 3600 * 24;
const hour = 1000 * 3600;
const minute = 1000 * 60;


@Pipe({name: 'timePast'})
export  class TimePast implements PipeTransform{


  transform(value: any, arg) : any{
    let dateStr = value;
    let utcTime = new Date(dateStr);
    let time = utcTime;//new Date(-new Date().getTimezoneOffset()*1000*60 + utcTime.getTime());

    let now = new Date();

    let past = now.getTime() - time.getTime();
    if(past < 0){
      past = 0;
    }


    let pastDay = Math.floor(past/day);
    if(pastDay > 0){
      return pastDay + " common.timeDayAgo";
    }

    let pastHour = Math.floor(past/hour);
    if(pastHour > 0){
      return pastHour + " common.timeHourAgo";
    }

    let pastMinute = Math.floor(past/minute);
    return pastMinute + " common.timeMinutesAgo";

  }



}





