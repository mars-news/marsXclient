/**
 * Created by meryn on 2017/06/07.
 */
import { Component } from '@angular/core';
import {NavController, ToastController, LoadingController, App, IonicPage} from 'ionic-angular';

import { MUser } from '../../providers/muser'

import {ErrorCode, MError} from "../../common/merror";
import {TabsPage} from "../tabs/tabs";
import { TranslateService } from '@ngx-translate/core';
import { mfuncs } from "../../common/mfuncs"

@IonicPage()
@Component({
  selector: 'page-interest-selection',
  templateUrl: 'interest-selection.html'
})
export class InterestSelectionPage {

  items : string[];
  rows : number[];

  private sel: boolean[];

  constructor(public app:App,
              public navCtrl: NavController,
              public userService: MUser,
              private toastCtl: ToastController,
              public loadingCtl: LoadingController,
              public translate: TranslateService) {

    this.items = [
      this.translate.instant("cat.socail"),
      this.translate.instant("cat.politics"),
      this.translate.instant("cat.world"),
      this.translate.instant("cat.economy"),
      this.translate.instant("cat.sports"),
      this.translate.instant("cat.tech"),
      this.translate.instant("cat.life"),
      this.translate.instant("cat.fashion"),
      this.translate.instant("cat.culture"),
      this.translate.instant("cat.health"),
      this.translate.instant("cat.education"),
      this.translate.instant("cat.magazine"),];

    this.rows = Array.from(Array(Math.ceil((this.items.length / 3))).keys());

    this.sel = Array(this.items.length)

    for(let ii = 0; ii < this.sel.length; ii++){
      this.sel[ii] = false;
    }
  }


  clickInterest(idx: number){


    this.sel[idx] = !this.sel[idx];

    mfuncs.log("clicked:" + idx)

  }


  clickOk(){

    mfuncs.log("click ok ");

    let intrs: number[] = [];

    for (let ii = 0; ii < this.sel.length; ii++){
      if(this.sel[ii]){
        intrs.push(ii)
      }
    }

    let intrS = intrs.join(",");

    let loader = this.loadingCtl.create({
      content: this.translate.instant("common.signin"),
      dismissOnPageChange: true
    });

    loader.present();

    this.userService.userRegister(intrS).then((uData) => {

      return loader.dismiss()

    }).then(() => {

      loader = this.loadingCtl.create({
        content: this.translate.instant("common.login"),
        dismissOnPageChange: true
      });
      loader.present();

      return this.userService.userLogin();

    }).then((ok) => {

      if (!ok){
        throw new MError(ErrorCode.Login_Failed, "LoginFailed");
      }

      return loader.dismiss()

    }).then(() => {

      this.app.getRootNav().setRoot("TabsPage");

      /*
      this.navCtrl.push(HomePage, {})
        .then(() => {
          let toast = this.toastCtl.create({
            message: "Login successed!",
            duration: 3000,
            position: 'bottom'
          });

          toast.present();
        });
      */

    }).catch((e) => {

      mfuncs.log("interest click failed" + e)

    });




  }
}











