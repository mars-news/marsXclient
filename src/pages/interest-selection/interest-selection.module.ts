/**
 * Created by meryn on 2017/06/23.
 */
import { InterestSelectionPage } from './interest-selection';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    InterestSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(InterestSelectionPage),
    TranslateModule.forChild()
  ],
  exports: [
    InterestSelectionPage
  ]
})

export class InterestSelectionPageModule { }
