/**
 * Created by meryn on 2017/06/13.
 */
import { Component, OnInit } from '@angular/core';
import {
  NavController, LoadingController, NavParams, ViewController, ModalController, ToastController,
  IonicPage
} from 'ionic-angular';
import { MUser } from '../../providers/muser'
import { MNet } from '../../providers/mnet'
import { MUrls } from '../../providers/murl'
import { MComment } from '../../providers/mcomment'
import { Comment } from '../../interfaces/comment'
import { CommentWritePage } from '../comment-write/comment-write'
import { TranslateService } from '@ngx-translate/core';
import { mfuncs } from "../../common/mfuncs";
import { MCache } from "../../providers/mcache";
import {RspErrorCode} from "../../common/merror";


@IonicPage()
@Component({
  selector: 'page-replies',
  templateUrl: 'replies.html'
})
export class RepliesPage implements OnInit {

  nid: number;
  comment: Comment;
  replies: Comment[];
  callback: any;
  page: any;

  constructor(public nav: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public modalCtrl: ModalController,
              public mnet: MNet,
              public murls: MUrls,
              public mcache: MCache,
              public mcomment: MComment,
              public translate: TranslateService) {
  }


  ngOnInit(){

    this.nid = this.navParams.get("nid");
    this.comment = this.navParams.get("comment");
    this.callback = this.navParams.get("callback");
    this.page = this.navParams.get("page");

    this.mcomment.getReplies(this.nid, this.comment.Id).then((replies) => {

      this.replies = replies;

    })
  }

  fetchNewReplies(infiniteScroll){

    this.mcomment.refreshReply(this.nid, this.comment.Id).then((replies) => {

      this.replies = replies;

      return true;
    }).then((val) => {
      infiniteScroll.complete();
    }).catch((err) => {
      mfuncs.log("fetch new replies failed:" + JSON.stringify(err));
    })
  }

  writeReply(event){

    let modalPage = this.modalCtrl.create("CommentWritePage", {nid:this.nid, pid:this.comment.Id, isReply:true});

    modalPage.onDidDismiss((ret) => {

      if(!ret || !ret.hasOwnProperty("Stat")){
        return
      }

      let msg : string;

      if(ret["Stat"] == 0){
        msg = this.translate.instant("newsDetail.wirteSuccess");

        this.replies.unshift(ret["comment"]);
        this.callback(this.page, this.comment.Id, this.replies.length);

        mfuncs.log("publish comment:" + JSON.stringify(ret["comment"]));

      }else if(ret["Stat"] == RspErrorCode.ErrorCode_User_Blocked){
        msg = this.translate.instant("newsDetail.writeFailedReport");
      }

      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'top'
      });
      toast.present();
    });

    modalPage.present();

  }

  clickVote($event, reply){
    mfuncs.log("click reply vote nid:" + reply.Nid + " pid:"+ reply.Pid + " rid:" + reply.Id);
    this.mcomment.addVote(reply.Nid, reply.Pid, reply.Id, true);
  }

  navBack(event){
    this.nav.pop()
  }

  clickReport(event, comment:Comment){
    this.mcomment.reportReply(comment.Nid, comment.Pid, comment.Id, comment.Uid)
  }

}













