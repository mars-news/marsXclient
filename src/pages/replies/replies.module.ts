/**
 * Created by meryn on 2017/06/23.
 */
import { RepliesPage } from './replies';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RepliesPage,
  ],
  imports: [
    IonicPageModule.forChild(RepliesPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    RepliesPage
  ]
})

export class RepliesPageModule { }
