import { Component } from '@angular/core';
import {IonicPage} from "ionic-angular";
import { mfuncs } from "../../common/mfuncs";


@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = "NewsListPage";
  tab2Root = "SettingsPage";

  constructor() {

  }

  ionViewDidLoad(){
    mfuncs.log("tab show");
  }
}
