/**
 * Created by meryn on 2017/06/23.
 */
import { ContactUsPage } from './contact-us';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ContactUsPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactUsPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    ContactUsPage
  ]
})

export class ContactUsPageModule { }































