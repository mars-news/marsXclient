import {Component, ViewChild} from '@angular/core';
import { ViewController, IonicPage, NavController} from 'ionic-angular';

import { mfuncs } from "../../common/mfuncs";
import {MNet} from "../../providers/mnet";


@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html'
})
export class ContactUsPage {

  agreementTxt: string;

  constructor(public navCtrl: NavController,
              private mnet: MNet,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {

    this.mnet.loadFile('assets/agreements/ja').then((content) => {

      this.agreementTxt = content;

    })


  }

  ionViewWillEnter(){
    this.viewCtrl.setBackButtonText('');

  }

}













