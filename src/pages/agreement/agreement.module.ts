/**
 * Created by meryn on 2017/06/23.
 */
import { AgreementPage } from './agreement';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AgreementPage,
  ],
  imports: [
    IonicPageModule.forChild(AgreementPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    AgreementPage
  ]
})

export class AgreementPageModule { }































