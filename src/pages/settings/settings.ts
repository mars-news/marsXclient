import {Component, ViewChild} from '@angular/core';
import { Platform, IonicPage, NavController, LoadingController, ActionSheetController, ModalController, AlertController, ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera'
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path'



import {UserData, UserNameExtInfo} from '../../interfaces/user-data'
import { MUser } from '../../providers/muser'
import { MDef } from "../../common/mdefs";
import { MUrls } from '../../providers/murl'
import { MError, ErrorCode } from '../../common/merror'
import { MUtils } from '../../common/mutils'
import { MCache } from '../../providers/mcache'
import {MImg} from "../../components/m-img/m-img";
import { TranslateService } from '@ngx-translate/core';
import { mfuncs } from "../../common/mfuncs";


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  @ViewChild('mimg') mPhoto: MImg;

  userName: string = '';
  userInfo: UserData;
  userEInfo: UserNameExtInfo;
  photover: number;
  version: number;


  constructor(public navCtrl: NavController,
              private platform: Platform,
              private loadingCtrl: LoadingController,
              private actionSheetCtrl: ActionSheetController,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private storage: Storage,
              private transfer: Transfer,
              private file: File,
              private filepath: FilePath,
              private camera:Camera,
              private muser:MUser,
              private murl:MUrls,
              private mcache: MCache,
              private plt:Platform,
              public translate: TranslateService) {


    this.muser.getMyPhotoVersion().then((ver) => {
      mfuncs.log("photo ver:" + ver);
      this.photover = ver;
    })
  }

  ionViewDidLoad() {

    this.muser.getUinfo().then((uinfo) => {

      this.userInfo = <UserData>uinfo;

      if(!this.userInfo){
        throw new MError(ErrorCode.Value_Missing, "user info is missing");
      }

      this.userName = this.userInfo.getNamePart();
      this.userEInfo = UserNameExtInfo.GetUserNameExtInfoFrom(this.userInfo.name);

    }).catch((err) => {

      mfuncs.log("get user info failed " + JSON.stringify(err));

    });

    this.storage.get(MDef.App_Version_Key).then((version) => {

      this.version = version;

    });
  }


  openUserInfoOptions() {
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant('settings.changeMenuTitle'),
      buttons: [
        {
          text: this.translate.instant('settings.changeName'),
          icon: 'md-browsers',
          handler: () => {
            this.changeUserName();
          }
        },
        {
          text: this.translate.instant('settings.changePhoto'),
          icon: 'ios-contact',
          handler: () => {
            this.openCamera(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
      ]
    });

    actionSheet.present();
  }


  openCamera(pictureSourceType: any) {
    var self = this;

    let options: CameraOptions = {
      quality: 95,
      //destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: pictureSourceType,
      //encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(imagePath => {


      let localFileName = this.createFileName();

      new Promise((resolve, _) => {

        mfuncs.log("get image path: " + imagePath + ' local name' + localFileName);

        if (this.platform.is('android') && pictureSourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filepath.resolveNativePath(imagePath)
            .then(filePath => {
              mfuncs.log('android path branch');

              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
              let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
              resolve(this.copyFileToLocalDir(correctPath, currentName, localFileName));
            });
        } else {
          mfuncs.log('non-android path branch');

          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          resolve(this.copyFileToLocalDir(correctPath, currentName, localFileName));
        }

      }).then(() => {

        return this.uploadImage(localFileName);

      }).then(() => {

        return true;
        //return this.file.removeFile(this.file.dataDirectory, localFileName);

      }).catch((err) => {
        mfuncs.log('ERROR -> ' + JSON.stringify(err));
      })
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName =  n + ".jpg";
    return newFileName;
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return this.file.dataDirectory + img;
    }
  }

// Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName):Promise<any> {

    mfuncs.log('image copy:' + namePath + ' ' + currentName + ' ' + this.file.dataDirectory + ' ' + newFileName);

    return this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName);
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,

      position: 'top'
    });
    toast.present();
  }


  public uploadImage(fileName:string):Promise<any> {

    // File for Upload
    var targetPath = this.pathForImage(fileName);

    // File name only
    var filename = fileName;

    var options = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };

    const fileTransfer: TransferObject = this.transfer.create();

    let loading = this.loadingCtrl.create({
      content: this.translate.instant('settings.uploading')
    });
    loading.present();

    let newName = "";

    // Use the FileTransfer to upload the image
    return fileTransfer.upload(targetPath, this.murl.getUploadPhotoUrl(), options).then(data => {
      mfuncs.log("upload image finished: " + JSON.stringify(data));
      loading.dismissAll();

      let jobj = JSON.parse(data.response);

      if(jobj &&  jobj['Body'] && jobj['Body']['UserName'] && jobj['Body']['UserName'].length>0){

        this.presentToast(this.translate.instant('settings.changePhotoSuccess'));
        newName = jobj['Body']['UserName'];

        mfuncs.log("upload image newName: " + newName);

        return this.muser.getUinfo();

      }else{
        throw new MError(ErrorCode.Response_Field_Missing, "upload response need UserName")

      }
    }).then((val) => {

      let uinfo = <UserData>val;
      if(!uinfo){
        throw new MError(ErrorCode.Load_User_From_Cache_Failed, "load user data failed")
      }
      uinfo.name = newName;

      this.userInfo = uinfo;

      mfuncs.log("new user info: " + JSON.stringify(this.userInfo));

      this.userEInfo = UserNameExtInfo.GetUserNameExtInfoFrom(uinfo.name);

      return this.muser.setUinfo(uinfo).then(() => {

        return this.muser.incrMyPhotoVersion().then((ver)=>{
          this.photover = ver;
          this.mPhoto.updateImageSubject.next(true);
        });

      });

    }).catch(err => {
      mfuncs.log("upload image failed" + JSON.stringify(err));
      loading.dismissAll();
      this.presentToast(this.translate.instant('settings.error'));
    });
  }




/*
  uploadPhoto(imageData:any):Promise<any>{

    let loader = this.loadingCtrl.create({
      content: 'Uploading image..',
    });
    loader.present();

    const fileTransfer: TransferObject = this.transfer.create();

    let options:FileUploadOptions = {
      fileKey: 'photo',
      fileName: this.userInfo.uid + ".jpg",
      mimeType: "image/jpeg",
      headers: {}
    };

    return fileTransfer.upload(imageData, this.murl.getUploadPhotoUrl(), options).then((val) => {
      mfuncs.log("upload successed:" + JSON.stringify(val));
      loader.dismiss();
    }).catch((err) => {
      mfuncs.log("upload failed:" + JSON.stringify(err));
      loader.dismiss();
    })

  }
*/

  changeUserName(){

    let alert = this.alertCtrl.create({
      title: this.translate.instant('settings.changeName'),
      message: this.translate.instant('settings.inputName'),
      inputs: [
        {
          name: 'Name',
          placeholder: this.translate.instant('settings.name')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('settings.cancel'),
          handler: (data: any) => {
            mfuncs.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('settings.ok'),
          handler: (data: any) => {
            mfuncs.log('Saved clicked ' + JSON.stringify(data));

            if(data['Name'] && MUtils.isString(data['Name']) && data['Name'].length > 0){
              this.updateUserName(data['Name']);
            }

          }
        }
      ]
    });

    alert.present();

  }


  updateUserName(newName: string){

    this.muser.userUpdateName(newName).then(() => {

      let toast = this.toastCtrl.create({
        message: this.translate.instant('settings.chaneNameSuccess'),
        duration: 2000,
        position: 'top'
      });
      toast.present();

      mfuncs.log("update uname succ:");

      return this.muser.getUinfo();

    }).then((val) => {

      let uinfo = <UserData>val;
      if(!uinfo){
        throw new MError(ErrorCode.Value_Missing, 'u info null');
      }

      this.userInfo = uinfo;
      this.userName = this.userInfo.getNamePart();

    }).catch((err) => {

      mfuncs.log("update uname failed" + JSON.stringify(err));

    })
  }

  onClickAgreement(){
    this.navCtrl.push("AgreementPage");
  }

  onClickContactUs(){
    this.navCtrl.push("ContactUsPage");
  }

}


















