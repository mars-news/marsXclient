/**
 * Created by meryn on 2017/06/23.
 */
import { SettingsPage } from './settings';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    SettingsPage
  ]
})

export class SettingsPageModule { }































