import { Component, OnInit, ViewChild } from '@angular/core';
import {
  NavController, NavParams, Events, LoadingController, ModalController, ToastController, Content,
  IonicPage
} from 'ionic-angular';
import { MNews } from '../../providers/mnews'
import {NewsItem, NewsSrc} from "../../interfaces/news";
import {Comment, Vote} from "../../interfaces/comment"
import { MComment } from '../../providers/mcomment'
import { CommentWritePage } from '../comment-write/comment-write'
import { RepliesPage } from '../replies/replies'
import { TranslateService } from '@ngx-translate/core';
import {MImg} from "../../components/m-img/m-img";
import { mfuncs } from "../../common/mfuncs"
import { InAppBrowser} from '@ionic-native/in-app-browser'
import {MCache} from "../../providers/mcache";
import { RspErrorCode } from "../../common/merror"

/**
 * Generated class for the NewsDetails page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-news-details',
  templateUrl: 'news-details.html',
})
export class NewsDetailsPage implements OnInit{

  @ViewChild(Content) content: Content;
  @ViewChild(MImg) srcImg: MImg;

  newsInfo: NewsItem;
  newsContent : any;
  comments: Comment[];

  oriUrl: string;

  refreshEnd = false;

  srcShowed = false;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController,
              public navParams: NavParams,
              private newsData: MNews,
              private mcache: MCache,
              private mcomment: MComment,
              public events: Events,
              public translate: TranslateService, public inAppBrowser: InAppBrowser) {
    /*
    this.newsData.getContent(this.newsInfo.FilePath)
      .then((data) => {
        this.newsContent = data
      })*/
  }

  ngOnInit(){
    let news = <NewsItem>this.navParams.data.News;

    if(!news){
      return;
    }

    this.newsInfo = news;

    this.oriUrl = news.Addr;


    let loader = this.loadingCtrl.create({
      content: this.translate.instant("common.loading"),
      dismissOnPageChange: true
    });

    loader.present();


    let commentCnt = 0;

    this.newsData.getContent(this.newsInfo.Id, this.newsInfo.FilePath).then((content) => {
        this.newsContent = content;
        return true;
    }).then((val) => {

        return this.mcomment.getComments(news.Id);
    }).then((comments) => {

      if(!comments || comments.length <= 0){
        this.refreshEnd = true;
      }

      this.comments = comments;

      mfuncs.log(/*"detail show:" + this.newsContent + */" comments count:" + this.comments.length + " ref end:" + this.refreshEnd);

      commentCnt = this.comments.length;

      return commentCnt;
    }).catch((err) => {
      mfuncs.log("init comment failed for nid:" + news.Id + JSON.stringify(err));
    });
  }

  ionViewDidLoad() {
    mfuncs.log('ionViewDidLoad NewsDetailsPage');
    this.events.publish('tab:change', false);
  }

  ionViewDidLeave() {

    mfuncs.log('leave detail:' + this.navCtrl.getActive().name);

  }


  fetchNewComments(infiniteScroll){

    let oldCount = this.mcomment.getCommentsCount(this.newsInfo.Id);

    this.mcomment.refreshComment(this.newsInfo.Id).then((comments) => {

      mfuncs.log("get comments count:" + comments.length);

      this.comments = comments;　　
      let newCount = this.comments.length;

      if (newCount <= oldCount){
        this.refreshEnd = true;
      }

      return true;
    }).then((val) => {
      infiniteScroll.complete();
    }).catch((err) => {
      mfuncs.log("err:" + JSON.stringify(err))
    })
  }

  scrollToTop() {
    var self = this;
    setTimeout(function () {
      self.content.scrollToTop();
    }, 1500);
  }

  navBack(event){

    this.navCtrl.pop();

    if(this.newsInfo){
      mfuncs.log("view leave detailed nid:" + this.newsInfo.Id);
      this.mcomment.SendScoreAndVotes(this.newsInfo.Id);
    }

  }


  writeComment(event){

    let modalPage = this.modalCtrl.create("CommentWritePage", {nid:this.newsInfo.Id, pid:0, isReply:false});

    modalPage.onDidDismiss((ret) => {

      if(!ret || !ret.hasOwnProperty("Stat")){
        return
      }

      let msg : string;

      if(ret["Stat"] == 0){
        msg = this.translate.instant("newsDetail.wirteSuccess");

        this.comments.unshift(ret["comment"]);
        this.newsData.updateCommentCnt(this.newsInfo.Id, this.comments.length);

        mfuncs.log("publish comment:" + JSON.stringify(ret["comment"]));

      }else if(ret["Stat"] == RspErrorCode.ErrorCode_User_Blocked){
        msg = this.translate.instant("newsDetail.writeFailedReport");
      }

      let toast = this.toastCtrl.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      toast.present();
    });

    modalPage.present();

  }

  clickComment(event, comment){

    mfuncs.log("click comment:" + comment.Id);

    this.navCtrl.push("RepliesPage", {nid: this.newsInfo.Id, comment:comment, callback: this.wrtieReplyCallback, page: this});

  }


  wrtieReplyCallback(page: any, pid:number, replyCount:number){

    mfuncs.log("write reply callback count:" + replyCount);

    for(let ii = 0; ii < page.comments.length; ii++){

      if(page.comments[ii].Id == pid){
        page.comments[ii].ReplyCount = replyCount;
      }
    }

  }

  clickVote(event, comment){

    mfuncs.log("click comment vote nid:" + this.newsInfo.Id + " pid:"+ comment.Id);

    this.mcomment.addVote(this.newsInfo.Id, comment.Id, 0, false);

  }


  onClickOriUrl(){

    let browser = this.inAppBrowser.create(this.oriUrl,'_system');

  }

  clickReport(event, comment:Comment){
    this.mcomment.reportComment(comment.Nid, comment.Id, comment.Uid).then(() => {

      return this.mcomment.getComments(this.newsInfo.Id);

    }).then((comments) => {
      this.comments = comments;
    })
  }




}


























