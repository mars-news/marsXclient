/**
 * Created by meryn on 2017/06/23.
 */
import { NewsDetailsPage } from './news-details';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NewsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsDetailsPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    NewsDetailsPage
  ]
})

export class NewsDetailsPageModule { }
