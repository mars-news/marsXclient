/**
 * Created by meryn on 2017/06/23.
 */

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {CommentWritePage} from "./comment-write";
import { TranslateModule } from '@ngx-translate/core';
import {SharedModule} from "../../app/shared.module";

@NgModule({
  declarations: [
    CommentWritePage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(CommentWritePage),
    TranslateModule.forChild()
  ],
  exports: [
    CommentWritePage
  ]
})

export class CommentWritePageModule { }
