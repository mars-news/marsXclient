/**
 * Created by meryn on 2017/06/13.
 */
import { Component, OnInit } from '@angular/core';
import {NavController, LoadingController, NavParams, ViewController, IonicPage} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl} from '@angular/forms';
import { MUser } from '../../providers/muser'
import { MNet } from '../../providers/mnet'
import { MUrls } from '../../providers/murl'
import { Comment } from '../../interfaces/comment'
import { MUtils } from '../../common/mutils'
import { UserData } from "../../interfaces/user-data";
import { MComment } from '../../providers/mcomment'
import { TranslateService } from '@ngx-translate/core';
import { mfuncs } from "../../common/mfuncs"
import { MWords } from "../../providers/mwords"

@IonicPage()
@Component({
  selector: 'comment-write',
  templateUrl: 'comment-write.html'
})
export class CommentWritePage implements OnInit{

  createCommentForm: FormGroup;
  comment: AbstractControl;
  loaded: boolean = false;

  nid: number;
  pid: number;
  isReply: boolean = false;

  constructor(public nav: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController,
              public fb: FormBuilder,
              public muser: MUser,
              public mnet: MNet,
              public murls: MUrls,
              public mwords: MWords,
              public mcomment: MComment,
              public translate: TranslateService) {

    this.nid = this.navParams.data.nid;
    this.pid = this.navParams.data.pid;
    this.isReply = this.navParams.data.isReply;

  }

  ngOnInit(){
    this.createCommentForm = this.fb.group({
      'comment': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])]
    });

    this.comment = this.createCommentForm.controls['comment'];
    this.loaded = true;
  }

  onSubmit(commentForm: any): void{

    if(this.createCommentForm.valid){

      let loader = this.loadingCtrl.create({
        content: this.translate.instant('settings.uploading'),
        dismissOnPageChange: true
      });

      loader.present();

      commentForm.comment = this.mwords.filterTxt(commentForm.comment);

      var userInfo: UserData;

      this.muser.getUinfo().then((uinfo) => {

        userInfo = uinfo;
        if(this.isReply){
          return this.mnet.send(this.murls.getCommentPublishUrl(),
            {uid: uinfo.uid, nid: this.nid, comment: commentForm.comment, reply: this.pid},
            ["Stat", "Pid"]).then((rsp) => {
            loader.dismiss();
            return rsp;
          })
        }else{
          return this.mnet.send(this.murls.getCommentPublishUrl(),
            {nid: this.nid, comment: commentForm.comment},
            ["Stat", "Pid"]).then((rsp) => {
            loader.dismiss();
            return rsp;
          })
        }

      }).then((rsp) => {

        mfuncs.log("published rsp :" + rsp.Stat);

        if(rsp.Stat != 0){
          this.viewCtrl.dismiss({Stat: rsp.Stat});
          return
        }

        this.mcomment.addScoreFromComment(this.nid);

        let newComment = new Comment();
        newComment.Id = rsp.Pid;
        newComment.Nid = this.nid;
        newComment.Pid = this.pid;
        newComment.Name = userInfo.name;
        newComment.Timestamp = MUtils.date2str(new Date());
        newComment.Nid = this.nid;
        newComment.Content = commentForm.comment;
        newComment.Uid = userInfo.uid;

        this.viewCtrl.dismiss({Stat: 0, comment: newComment});

      }).catch((err) =>{

        mfuncs.log("publish comment failed" + JSON.stringify(err));

        this.viewCtrl.dismiss({Stat: 0, comment: null});

      })
    }
  }


  cancelNewComment() {
    this.viewCtrl.dismiss();
  }

}































