/**
 * Created by meryn on 2017/06/23.
 */
import { NewsListPage } from './news-list';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NewsListPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsListPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    NewsListPage
  ]
})

export class NewsListPageModule { }
