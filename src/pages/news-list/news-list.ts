/**
 * Created by meryn on 2017/04/18.
 */
import {Component, ViewChild} from '@angular/core';
import {NavController, LoadingController, Loading, NavParams, AlertController, Events, App, IonicPage} from 'ionic-angular';


import { MNews } from '../../providers/mnews'
import { NewsDetailsPage } from '../news-details/news-details'
import { MUrls } from '../../providers/murl'
//import { MSlide } from '../../components/m-slide/m-slide'
import {NewsItem} from "../../interfaces/news";
import {ErrorCode, MError} from "../../common/merror";
import { TranslateService } from '@ngx-translate/core';
import { mfuncs } from "../../common/mfuncs";

/*
 Generated class for the ParkList page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */

declare var FCMPlugin;

@IonicPage()
@Component({
  selector: 'page-news-list',
  templateUrl: 'news-list.html'
})
export class NewsListPage {

  @ViewChild('tabs') tabs;


  currCat : number = 0;
  newsList: NewsItem[] = null;
  //imgData: any = null;
  pageCats:string[] = ["头条", "社会", "国内", "国际", "娱乐", "体育", "军事", "科技", "财经", "时尚"];

  isEmptyPage: boolean = true;
  emptyPages: number[] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public navParams: NavParams,
              public newsData: MNews,
              public murl: MUrls,
              public alertCtrl: AlertController,
              public loadCtrl: LoadingController,
              public events:Events,
              public app:App,
              public translate: TranslateService) {
  }



  ngOnInit() {

    mfuncs.log("ng on init");

    this.pageCats = [this.translate.instant("cat.suggest"),
      this.translate.instant("cat.socail"),
      this.translate.instant("cat.politics"),
      this.translate.instant("cat.world"),
      this.translate.instant("cat.economy"),
      this.translate.instant("cat.sports"),
      this.translate.instant("cat.tech"),
      this.translate.instant("cat.life"),
      this.translate.instant("cat.culture"),
      this.translate.instant("cat.health"),
      this.translate.instant("cat.education"),
      this.translate.instant("cat.magazine"),
      this.translate.instant("cat.column"),
      this.translate.instant("cat.entertainment"),
    ];

    let loader = this.loadingCtrl.create({
      content: this.translate.instant("common.loading"),
      dismissOnPageChange: true
    });

    loader.present();


    this.initNews().then((newsLen) => {
      mfuncs.log("init news len:" + newsLen);

      return this.newsData.getAllNewsByCat(0).then((list) => {

        this.newsList = <NewsItem[]>list;

        if(this.newsList && this.newsList.length > 0){
          this.isEmptyPage = false;
        }else{
          return false;
        }

        mfuncs.log("curr show length:" + list.length);

        return true
      })
    }).then((val) => {

      this.events.subscribe("cat:change", this.changeNewsCat);

      loader.dismiss();
      mfuncs.log("init news successed");

    }).catch((err) => {

      loader.dismiss();
      mfuncs.log("init news failed" + err);

    });


    if(typeof(FCMPlugin) !== "undefined"){
      FCMPlugin.getToken(function(t){
        console.log("Use this token for sending device specific messages\nToken: " + t);
      }, function(e){
        console.log("Uh-Oh!\n"+e);
      });

      FCMPlugin.onNotification(function(d){
        if(d.wasTapped){
          // Background recieval (Even if app is closed),
          //   bring up the message in UI
        } else {
          // Foreground recieval, update UI or what have you...
        }
        console.log(d + JSON.stringify(d));
      }, function(msg){
        // No problemo, registered callback
        console.log(msg + JSON.stringify(msg));

      }, function(err){
        console.log("Arf, no good mate... " + err);
      });
    } else console.log("Notifications disabled, only provided in Android/iOS environment");



  }



  public changeNewsCat = (catid) => {
    this.isEmptyPage = true;

    return this.newsData.getAllNewsByCat(catid).then((list) => {
      this.newsList = list;
      this.currCat = catid;
      if(list && list.length > 0){
        this.isEmptyPage = false;
      }

    }).catch((e) => {
      mfuncs.log("change news cat failed" + JSON.stringify(e))

    })
  };


  doRefresh(refresher){

    this.newsData.refresh(this.currCat).then((list) => {
      if(list && list.length > 0){
        this.isEmptyPage = false;
      }
      this.newsList = list;
      refresher.complete();
    })
  }




  initNews():Promise<number>{

    if(this.newsData.isInitingNews){
      mfuncs.log('another process initing');

      return Promise.resolve().then(() => {

        throw new MError(ErrorCode.Value_Repeated, 'init news twice')

      });
    }

    this.newsData.isInitingNews = true;

    if(!this.newsData.hasRemainingNews()){

      return this.newsData.InitNews()
        .then((newsLength) =>{
          return this.newsData.InitSrcs()
            .then((val) => {
              this.newsData.isInitingNews = false;
              return newsLength;
            })
        })
    }

    return Promise.resolve(this.newsData.getRemainingNewsLength())
  }



  getTitleImageUrl(news){
    mfuncs.log("get title pic:" + this.murl.getNewsStaticUrl() + '/' + news.TitleImage);

    return this.murl.getNewsStaticUrl() + '/' + news.TitleImage;
  }


  onSlideClick(index) {
    mfuncs.log("slide click:" + index);
    this.events.publish("cat:change", index);

  }



  getNewsDetails(news){
    mfuncs.log("get content:" + news.Id);

    //this.navCtrl.push(NewsDetailsPage, {News: news});
    this.app.getRootNav().push("NewsDetailsPage", {News: news})

  }

}






















