/**
 * Created by meryn on 2017/04/21.
 */


export class NewsItem{

  Id: number;
  RuleId: number;
  SourceId: number;
  ShowKind: number;
  Addr: string;
  Title: string;
  TitleImage: string;
  TitleImageText: string;
  Category: number;
  Feature: number;
  FilePath: string;
  Timestamp: string;
  DownloadTime: string;

  //client//
  RefreshCat: number;
  CommentCnt: number;

  static initFromJObj(jobj){
    let obj = new NewsItem();
    for(let mem in jobj){
      obj[mem] = jobj[mem];
    }
    return obj;
  }
}

export class NewsSrc{
  Id: number;
  Name: string;
  ShowName: string;
  PicPath: string;

  static initFromJObj(jobj){
    let obj = new NewsSrc();
    for(let mem in jobj){
      obj[mem] = jobj[mem];
    }
    return obj;
  }

}
