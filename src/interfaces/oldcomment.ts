/**
 * Created by meryn on 2017/05/08.
 */

export class OneComment{

  Base: OldComment;

  Last1: BuildComment;
  Last2: BuildComment;
  Last3: BuildComment;
  LastArr: BuildComment[];

  HasMore: boolean


}

export class BuildComment{

  Id: number;
  Uid: number;
  Nid: number;
  Name: string;
  Content: string;
  Vote: number;
  FloorIndex: number;

  constructor(jobj){

    for(let mem in jobj){
      this[mem] = jobj[mem];
    }
  }

}


export class OldComment{

  Id: number;
  Uid: number;
  Nid: number;
  Name: string;
  Timestamp: string;
  Content: string;
  Vote: number;

  BuildIds: number[];


  constructor(jobj){

    for(let mem in jobj){
      if(mem == "BuildIds"){

        this.BuildIds = [];

        for(let ii = 0; ii < jobj[mem].length; ii++){
          this.BuildIds.push(jobj[mem][ii])
        }

      }else{
        this[mem] = jobj[mem];
      }
    }
  }
}
