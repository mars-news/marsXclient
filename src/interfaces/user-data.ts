/**
 * Created by meryn on 2017/04/22.
 */


export class UserNameExtInfo{

  name: string;
  hasPhoto: boolean;

  constructor(){
    this.hasPhoto = false;
  }

  static GetUserNameExtInfoFrom = function  (name: string): UserNameExtInfo{

    let extInfo = new UserNameExtInfo();

    let regx = /([^\(\)]+)\((.+)\)/;

    let groups = name.match(regx);

    if(!groups || groups.length < 3){
      return null;
    }

    let name_str = groups[1];

    extInfo.name = name_str;

    let info_str = groups[2];

    let infos = info_str.split(',');

    if(infos[0] == "t"){
      extInfo.hasPhoto = true;
    }

    return extInfo;

  }

}

export class UserData{

  uid: number;
  token: string;
  name: string;
  photoUrl: string;
  interestStr: string;

  public static initFromJObj(jobj){
    let obj = new UserData();
    for(let mem in jobj){
      obj[mem] = jobj[mem];
    }
    return obj;
  }

  getNamePart(): string{
    let extInfo = UserNameExtInfo.GetUserNameExtInfoFrom(this.name);
    if(!extInfo){
      return "";
    }
    return extInfo.name;
  }

}




