/**
 * Created by meryn on 2017/05/08.
 */



export class Comment{

  Id: number;
  Uid: number;
  Nid: number;
  Pid: number;
  Name: string;
  Timestamp: string;
  Content: string;
  Vote: number;
  ReplyCount: number;
  HasVote: boolean;
  Reported: boolean;

  constructor(){
    this.Vote = 0;
    this.ReplyCount = 0;
  }

  public static initFromJObj(jobj){
    let obj = new Comment();
    for(let mem in jobj){
      obj[mem] = jobj[mem];
    }
    return obj;
  }
}


export class Vote{

  Pid: number;
  Rid: number;
  IsReply: boolean;

  constructor(pid:number, rid:number, reply:boolean){
    this.Pid = pid;
    this.Rid = rid;
    this.IsReply = reply;
  }

}




