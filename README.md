[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

## A News App

This is a mobile client for a news app developed in Iconic SDK.
The server-side is located at https://gitlab.com/mars-news/NewsServer

1. Developed using the Ionic SDK, cross-platform for iOS and Android.
2. The code is based on the AngularJS framework.
3. Written in TypeScript with strong typing and object-oriented style.
4. The coding style is based on reactive programming with RxJS.


[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555


[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-url]: https://www.linkedin.com/in/shaokun-feng/

